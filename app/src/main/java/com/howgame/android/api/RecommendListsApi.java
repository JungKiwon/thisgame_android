package com.howgame.android.api;

import com.howgame.android.model.RecommendListsModel;

/**
 * Created by Administrator on 2016-09-20.
 */
public class RecommendListsApi extends BaseApi {

    private RecommendListsModel result;

    public RecommendListsModel getResult() {
        return result;
    }

    public void setResult(RecommendListsModel result) {
        this.result = result;
    }
}
