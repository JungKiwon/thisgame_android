package com.howgame.android.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.howgame.android.R;

import static com.bumptech.glide.load.DecodeFormat.PREFER_ARGB_8888;

/**
 * Created by Administrator on 2016-08-22.
 */
public class LoadingDialog extends Dialog {

    private ImageView ivLoading;
    private TextView tvLoading;

    public LoadingDialog(Context context, String msg) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.loading_dialog);
        ivLoading = (ImageView) findViewById(R.id.ivLoading);
        tvLoading = (TextView) findViewById(R.id.tvLoading);
        tvLoading.setText(msg);
        setCancelable(false);
        Glide.with(context).load(R.drawable.loading_ani)
                .asGif()
                .dontTransform()
                .into(ivLoading);
    }

    public LoadingDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected LoadingDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


}
