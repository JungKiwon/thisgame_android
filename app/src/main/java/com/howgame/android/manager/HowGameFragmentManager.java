package com.howgame.android.manager;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * 프래그먼트 Add, Replace 매니저
 */
public class HowGameFragmentManager {

    private int mContainerId;
    private Activity mActivity;

    public HowGameFragmentManager(Activity activity, int containerId) {
        mActivity = activity;
        mContainerId = containerId;
    }

    /**
     * 프래그먼트 add
     *
     * @param fm             프래그먼트 매니저
     * @param fragment       프래그먼트
     * @param addToBackStack 백스택 사용 여부
     */
    public void add(FragmentManager fm, Fragment fragment, boolean addToBackStack) {

        if (fragment == null)
            return;

        FragmentTransaction ft = fm.beginTransaction();


        ft.add(mContainerId, fragment);

        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        ft.commit();
    }

    /**
     * 프래그먼트 replace
     *
     * @param fm             프래그먼트 매니저
     * @param fragment       프래그먼트
     * @param addToBackStack 백스택 사용 여부
     */
    public void replace(FragmentManager fm, Fragment fragment, boolean addToBackStack) {

        if (fragment == null)
            return;

        FragmentTransaction ft = fm.beginTransaction();
//        if (fm.getBackStackEntryCount() > 0) {
//            fm.popBackStack();
//        } else {
            ft.replace(mContainerId, fragment);
            if (addToBackStack) {
                ft.addToBackStack(null);
            }
            ft.commitAllowingStateLoss();
//        }
    }

    /**
     * 프래그먼트 제거
     */
    public void popBackStack() {
        mActivity.getFragmentManager().popBackStack();
    }

}
