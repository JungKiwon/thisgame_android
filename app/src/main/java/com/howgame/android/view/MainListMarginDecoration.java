package com.howgame.android.view;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import com.howgame.android.adapter.MainListAdapter;
import com.howgame.android.adapter.RelativeListAdapter;

/**
 * RecyclerView Decoration
 */
public class MainListMarginDecoration extends RecyclerView.ItemDecoration {
    private int margin;

    public MainListMarginDecoration(Context context, int margin) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        this.margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, margin, metrics);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        MainListAdapter adapter = (MainListAdapter) parent.getAdapter();
        if (adapter.hasHeader()) {
            if (parent.getChildAdapterPosition(view) % 2 == 1) {
                outRect.set(0, margin, margin, 0);
            } else if (parent.getChildAdapterPosition(view) % 2 == 0) {
                outRect.set(margin, margin, margin, 0);
            }
        } else {
            if (parent.getChildAdapterPosition(view) % 2 == 1) {
                outRect.set(0, margin, margin, 0);
            } else if (parent.getChildAdapterPosition(view) % 2 == 0) {
                outRect.set(margin, margin, margin, 0);
            }
        }
    }
}
