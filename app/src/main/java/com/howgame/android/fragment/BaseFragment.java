package com.howgame.android.fragment;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.UserManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.howgame.android.api.BaseApi;
import com.howgame.android.model.BaseModel;
import com.howgame.android.property.PrefsManager_;
import com.howgame.android.util.HowGameUtil;
import com.howgame.android.util.LogUtil;
import com.howgame.android.view.CustomSwipeRefreshLayout;
import com.howgame.android.view.HowGameProgressDialog;
import com.howgame.android.view.LoadingDialog;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-08-23.
 */
@EFragment
public class BaseFragment extends Fragment {

    @Pref
    PrefsManager_ mPrefsManager;

//    @Bean
//    LoadingManager mLoadingManager;

    protected Context mContext;
    protected AppCompatActivity mActivity;
    protected HowGameProgressDialog mProgressDlg;
    protected String mNextCursor = "";

    // 각 프래그먼트 용도에 사용 가능한 데이터들
    protected ArrayList mListData;


    /**
     * 페이징 정보 초기화
     */
    protected void clearCursor() {
        LogUtil.d("clear cursor");
        mNextCursor = "";
        mEndPage = false;
    }

    protected CustomSwipeRefreshLayout mSwipeRefreshLayout;

    /**
     * SwipeRefreshLayout 설정
     * <p/>
     * refresh 중에는 로딩 다이얼로그를 표시하지 않도록 하기 위해 사용
     *
     * @param layout { parameter_description }
     */
    protected void setSwipeRefreshLayout(CustomSwipeRefreshLayout layout) {
        mSwipeRefreshLayout = layout;
    }

    /**
     * 다음 페이지 정보를 리턴
     *
     * @return
     */
    protected String getNextCursor() {
        return HowGameUtil.defaultString(mNextCursor);
    }

    protected boolean mEndPage = false;

    /**
     * 마지막 페이지인지 체크
     *
     * @return
     */
    protected boolean isEndPage() {
        return mEndPage;
    }

    /**
     * 새로 받은 cursor 값으로 마지막 페이지인지 체크
     * <p/>
     * 빈 값이면 마지막이라고 판단
     */
    protected void checkEndPage() {
        mEndPage = HowGameUtil.isEmpty(getNextCursor());
    }

    /**
     * 다음 cursor 저장
     *
     * @param cursor
     */
    protected void setNextCursor(String cursor) {
        mNextCursor = HowGameUtil.defaultString(cursor);
    }

    /**
     * 화면 세로 사이즈 리턴
     *
     * @return
     */
    protected int getScreenHeight() {
        mActivity = (AppCompatActivity) getActivity();
        if (mActivity == null) {
            return 0;
        }
        return mActivity.findViewById(android.R.id.content).getHeight();
    }

    @Override
    public void onResume() {
        super.onResume();

//        mActivity = (AppCompatActivity) getActivity();
//        mLoadingManager.setContext(mActivity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContext = getActivity().getApplicationContext();
        mActivity = (AppCompatActivity) getActivity();

//        EventBusManager.register(this);
    }

    @Override
    public void onDestroyView() {
//        EventBusManager.unregister(this);
        super.onDestroyView();
    }

    /**
     * 프래그먼트 종료
     */
    protected void finishFragment() {
        try {
            getChildFragmentManager().popBackStack();
        } catch (Exception e) {
        }
    }

    /**
     * 키보드 숨김
     */
    public void hideKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mActivity.getWindow().getDecorView().getRootView().getWindowToken(), 0);
        } catch (Exception e) {
            if (LogUtil.ENABLE) {
                LogUtil.e(e);
            }
        }
    }


    /**
     * 로딩 다이얼로그 종료
     */
    public void dismiss() {

//        mLoadingManager.dismiss();
        try {
            if (mProgressDlg != null && mProgressDlg.isShowing()) {
                mProgressDlg.dismiss();
                mProgressDlg = null;
            }
        } catch (Exception e) {
            LogUtil.e(e);
        }
    }

    /**
     * 로딩 다이얼로그 실행
     *
     * @param msg { parameter_description }
     */
    public void show(String msg) {

        if (mActivity == null || mActivity.isFinishing())
            return;

        // swipe refresh layout 동작중이면 표시하지 않음
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing())
            return;

//        mLoadingManager.show(msg);
        if (HowGameUtil.isEmpty(msg)) {
            msg = "로드중...";
        }

        if (mProgressDlg == null || !mProgressDlg.isShowing()) {
            try {
                mProgressDlg = new HowGameProgressDialog(mActivity, msg);
                mProgressDlg.show();
            } catch (Exception e) {
                LogUtil.e(e);
            }
        }
    }

    /**
     * FragmentEvent 이벤트 핸들러
     * <p/>
     * 모든 프래그먼트가 전달받는 이벤트
     * <p/>
     * 현재 사용되지 않음
     *
     * @param event { parameter_description }
     */
//    public void onEventMainThread(EventBusManager.FragmentEvent event) {
//    }


//    @Override
//    public void onSaveInstanceState(Bundle outState) {
////        super.onSaveInstanceState(outState);
//    }
}
