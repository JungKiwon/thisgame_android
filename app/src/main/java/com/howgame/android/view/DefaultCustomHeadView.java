package com.howgame.android.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import com.howgame.android.util.HowGameUtil;
import com.howgame.android.R;
import com.howgame.android.util.LogUtil;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by liu on 2014/9/15.
 */

/**
 * The DefaultCustomHeadViewLayout is a refresh head view provided as default.
 * You can also make your own head view layout which must implement
 * CustomSwipeRefreshHeadview.CustomSwipeRefreshHeadLayout interface.
 */
public class DefaultCustomHeadView extends LinearLayout implements CustomSwipeRefreshLayout.CustomSwipeRefreshHeadLayout {

    private Context mContext;
    private LinearLayout mContainer;

//    private TextView mMainTextView;
//    private TextView mSubTextView;
//    private ImageView mImageView;
//    private ProgressBar mProgressBar;

    private Animation mRotateUpAnim;
    private Animation mRotateDownAnim;
    private final int ROTATE_ANIM_DURATION = 180;
    private AnimatedImageView ivSatellite;
    private Animation.AnimationListener animationListener;


    public DefaultCustomHeadView(Context context) {
        super(context);
        setWillNotDraw(false);
        mContext = context;
        setupLayout();
    }

    public void setupLayout() {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                HowGameUtil.getPX(mContext, 70));
        mContainer = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.update_ani, null);
        addView(mContainer, lp);
        setGravity(Gravity.BOTTOM);

    }

    public void setupAnimation() {

//
//        mRotateUpAnim = new RotateAnimation(0.0f, -180.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        Animation.AnimationListener mRotateUpAnimListener = animationListener;
//        mRotateUpAnim.setAnimationListener(mRotateUpAnimListener);
//        mRotateUpAnim.setDuration(ROTATE_ANIM_DURATION);
//        mRotateUpAnim.setFillAfter(true);
//
//        mRotateDownAnim = new RotateAnimation(-180.0f, 0.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//        mRotateDownAnim.setDuration(ROTATE_ANIM_DURATION);
//        mRotateDownAnim.setFillAfter(true);
    }

    @Override
    public void onStateChange(CustomSwipeRefreshLayout.State state, CustomSwipeRefreshLayout.State lastState) {
        int stateCode = state.getRefreshState();
        int lastStateCode = lastState.getRefreshState();
//        LogUtil.d(state.toString());
        if (stateCode == lastStateCode) {
            return;
        }
        if (stateCode == CustomSwipeRefreshLayout.State.STATE_COMPLETE) {
            LogUtil.d("onStateChange STATE_COMPLETE");
            ivSatellite.clearAnimation();
//            mImageView.clearAnimation();
//            mImageView.setVisibility(View.INVISIBLE);
//            mProgressBar.setVisibility(View.INVISIBLE);
        } else if (stateCode == CustomSwipeRefreshLayout.State.STATE_REFRESHING) {
            LogUtil.d("onStateChange STATE_REFRESHING");
            // show progress
            ivSatellite.clearAnimation();
//            mImageView.clearAnimation();
//            mImageView.setVisibility(View.INVISIBLE);
//            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            LogUtil.d("onStateChange STATE else");
            // show arrow
//            mImageView.setVisibility(View.VISIBLE);
//            mProgressBar.setVisibility(View.INVISIBLE);
        }

        switch (stateCode) {
            case CustomSwipeRefreshLayout.State.STATE_NORMAL:
                LogUtil.d("onStateChange switch STATE_NORMAL");
//                ivSatellite.setImageResource(R.anim.pull_to_refresh);
                break;
            case CustomSwipeRefreshLayout.State.STATE_READY:
                LogUtil.d("onStateChange switch STATE_READY");
                ivSatellite.stop();
                break;
            case CustomSwipeRefreshLayout.State.STATE_REFRESHING:
                LogUtil.d("onStateChange switch STATE_REFRESHING");
                ivSatellite.start();
                break;

            case CustomSwipeRefreshLayout.State.STATE_COMPLETE:
                LogUtil.d("onStateChange switch STATE_COMPLETE");
                ivSatellite.stop();
                ivSatellite.clearAnimation();
                break;
            default:
        }
    }

    public void updateData() {

        String time = fetchData();
        if (time != null) {
//            mSubTextView.setVisibility(VISIBLE);
//            mSubTextView.setText(time);
        } else {
//            mSubTextView.setVisibility(GONE);
        }

    }

    public String fetchData() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }


}