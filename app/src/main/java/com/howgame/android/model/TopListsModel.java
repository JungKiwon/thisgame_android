package com.howgame.android.model;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-09-20.
 */
public class TopListsModel extends BaseModel {

    private ArrayList<TopInfos> top_infos;
    private int total_count;

    public ArrayList<TopInfos> getTop_infos() {
        return top_infos;
    }

    public void setTop_infos(ArrayList<TopInfos> top_infos) {
        this.top_infos = top_infos;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public class TopInfos {
        private String package_name;
        private String title;
        private String thumbnail_url;

        public String getPackage_name() {
            return package_name;
        }

        public void setPackage_name(String package_name) {
            this.package_name = package_name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThumbnail_url() {
            return thumbnail_url;
        }

        public void setThumbnail_url(String thumbnail_url) {
            this.thumbnail_url = thumbnail_url;
        }
    }

}
