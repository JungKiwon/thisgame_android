package com.howgame.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.howgame.android.service.HowGameService;
import com.howgame.android.util.LogUtil;

/**
 * Created by Administrator on 2016-09-26.
 */

public class HowGameBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.d("Service Stops! Oooooooooooooppppssssss!!!!");
        context.startService(new Intent(context, HowGameService.class));
    }

}
