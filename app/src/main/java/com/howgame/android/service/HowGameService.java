package com.howgame.android.service;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.howgame.android.R;
import com.howgame.android.api.BaseApi;
import com.howgame.android.property.Constants;
import com.howgame.android.property.NetworkProperty;
import com.howgame.android.property.PrefsManager_;
import com.howgame.android.util.AES256Cipher;
import com.howgame.android.util.LogUtil;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.androidannotations.annotations.EService;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 2016-09-26.
 */

@EService
public class HowGameService extends Service {
    @Pref
    PrefsManager_ mPrefsManager;

    public int counter = 0;
    private Timer timer;
    private TimerTask timerTask;
    long oldTime = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LogUtil.d("EXIT");
        Intent broadcastIntent = new Intent("com.howgame.android.RestartService");
        sendBroadcast(broadcastIntent);
        stoptimertask();
    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //to wake up every 1 hour
        timer.schedule(timerTask, Constants.SEND_INSTALL_PACKAGE_TIME, Constants.SEND_INSTALL_PACKAGE_TIME); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
//                Log.i("in timer", "in timer ++++  " + (counter++));
                getInstalledAppList();
            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }


    private void getInstalledAppList() {
        LogUtil.d("getInstalledAppList");
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> apps = pm.getInstalledApplications(0);
        List<ApplicationInfo> installedApps = new ArrayList<ApplicationInfo>();

        for (ApplicationInfo app : apps) {
            //checks for flags; if flagged, check if updated system app
            if ((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                LogUtil.d(app.toString());
                installedApps.add(app);
                //it's a system app, not interested
            } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                //Discard this one
                //in this case, it should be a user-installed app
            } else {
                installedApps.add(app);
            }
        }
        sendInstalledAppList(installedApps);
    }

    private void sendInstalledAppList(List<ApplicationInfo> list) {
        String installedGames = "";
        for (int i = 0; i < list.size(); i++) {
            installedGames = installedGames + (i == 0 ? "" : ",") + list.get(i).processName;
        }
        LogUtil.d("getInstalledAppList url : " + NetworkProperty.HOST + NetworkProperty.API_INSTALL_ADD);
        LogUtil.d("getInstalledAppList auth_token : " + mPrefsManager.authToken().get());
        LogUtil.d("getInstalledAppList install_games : " + installedGames);

        String encodeParam = "";
        JSONObject obj = new JSONObject();
        try {
            obj.put("auth_token", mPrefsManager.authToken().get());
            obj.put("install_games", installedGames);
            LogUtil.d("params : " + obj.toString());
            encodeParam = AES256Cipher.AES_Encode(obj.toString(), Constants.AES_KEY);
            LogUtil.d("encodeParam : " + encodeParam);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObject object = new JsonObject();
        object.addProperty("method", NetworkProperty.METHOD_INSTALL_ADD);
        object.addProperty("params", encodeParam);

        Ion.with(this)
                .load(NetworkProperty.HOST + "/jsonrpc")
                .setTimeout(Constants.TIME_OUT)
                .addHeader("Content-Type", "application/json")
                .setJsonObjectBody(object)
                .as(new TypeToken<JsonObject>() {
                })
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject ret) {
                        if (e != null)
                            LogUtil.e(e);

                        if (ret != null) {
                            LogUtil.d("getInstalledAppList msg : " + ret.toString());
                            initGame();
                        }
                    }
                });
    }

    private void initGame() {
        LogUtil.d("initGame url : " + NetworkProperty.HOST + NetworkProperty.API_GAME_INIT);
        LogUtil.d("initGame auth_token : " + mPrefsManager.authToken().get());
        Ion.with(this)
                .load(NetworkProperty.HOST + NetworkProperty.API_GAME_INIT)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                .as(new TypeToken<BaseApi>() {
                })
                .setCallback(new FutureCallback<BaseApi>() {
                    @Override
                    public void onCompleted(Exception e, BaseApi ret) {
                        if (e != null) {
                            LogUtil.e(e);
                        }

                        if (ret != null) {
                            LogUtil.d("initGame msg : " + ret.getError_message());
                        }
                    }
                });
    }
}
