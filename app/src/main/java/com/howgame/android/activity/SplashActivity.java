package com.howgame.android.activity;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.Menu;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.howgame.android.MainActivity_;
import com.howgame.android.R;
import com.howgame.android.analytics.HowGameAnalyticsManager;
import com.howgame.android.api.BaseApi;
import com.howgame.android.api.CreateApi;
import com.howgame.android.model.CreateModel;
import com.howgame.android.property.Constants;
import com.howgame.android.property.NetworkProperty;
import com.howgame.android.util.AES256Cipher;
import com.howgame.android.util.LogUtil;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.JSONObjectBody;
import com.koushikdutta.ion.Ion;

import org.androidannotations.annotations.EActivity;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016-08-11.
 */
@EActivity(R.layout.activity_splash)
public class SplashActivity extends BaseActivity {

    Handler mFinishHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            LogUtil.d("mFinishHandler : " + msg);
            MainActivity_.intent(mContext).start();
            finish();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.d("onCreate");

        new HowGameAnalyticsManager(this).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_SPALSH);

        String authToken = mPrefsManager.authToken().get();
        if (mPrefsManager.authToken().get() == null || authToken.length() == 0) {
            sendDeviceInfo();
        } else {
            LogUtil.d("auth_token exists : " + mPrefsManager.authToken().get());
//            FirebaseInstanceId.getInstance().getToken();
            getInstalledAppList();
        }

    }


    private void getInstalledAppList() {
        LogUtil.d("getInstalledAppList");
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> apps = pm.getInstalledApplications(0);
        List<ApplicationInfo> installedApps = new ArrayList<ApplicationInfo>();

        for (ApplicationInfo app : apps) {
            //checks for flags; if flagged, check if updated system app
            if ((app.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0) {
                LogUtil.d(app.toString());
                installedApps.add(app);
                //it's a system app, not interested
            } else if ((app.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
                //Discard this one
                //in this case, it should be a user-installed app
            } else {
                installedApps.add(app);
            }
        }
        sendInstalledAppList(installedApps);
    }

    private void sendInstalledAppList(List<ApplicationInfo> list) {
        String installedGames = "";
        for (int i = 0; i < list.size(); i++) {
            installedGames = installedGames + (i == 0 ? "" : ",") + list.get(i).processName;
        }
        LogUtil.d("getInstalledAppList url : " + NetworkProperty.HOST + NetworkProperty.API_INSTALL_ADD);
        LogUtil.d("getInstalledAppList auth_token : " + mPrefsManager.authToken().get());
        LogUtil.d("getInstalledAppList install_games : " + installedGames);

        String encodeParam = "";
        JSONObject obj = new JSONObject();
        try {
            obj.put("auth_token", mPrefsManager.authToken().get());
            obj.put("install_games", installedGames);
            LogUtil.d("params : " + obj.toString());
            encodeParam = AES256Cipher.AES_Encode(obj.toString(), Constants.AES_KEY);
            LogUtil.d("encodeParam : " + encodeParam);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObject object = new JsonObject();
        object.addProperty("method", NetworkProperty.METHOD_INSTALL_ADD);
        object.addProperty("params", encodeParam);

        Ion.with(this)
                .load(NetworkProperty.HOST + "/jsonrpc")
                .setTimeout(Constants.TIME_OUT)
                .addHeader("Content-Type", "application/json")
                .setJsonObjectBody(object)
                .as(new TypeToken<JsonObject>() {
                })
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject ret) {
                        if (e != null)
                            LogUtil.e(e);

                        if (ret != null) {
                            LogUtil.d("getInstalledAppList msg : " + ret.toString());
                        }
                        mFinishHandler.sendEmptyMessageDelayed(0, 2000);
                    }
                });
    }

    private void sendDeviceInfo() {
        LogUtil.d("sendDeviceInfo url : " + NetworkProperty.HOST + NetworkProperty.API_DEVICE_CREATE);
        LogUtil.d("sendDeviceInfo device_id : " + Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
        LogUtil.d("sendDeviceInfo model_name : " + Build.MODEL.toString());
        LogUtil.d("sendDeviceInfo platform : " + Constants.PLATFORM);
        LogUtil.d("sendDeviceInfo os_version : " + Constants.OS_VERSION);
        String encodeParam = "";
        JSONObject obj = new JSONObject();
        try {
            obj.put("device_id", Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
            obj.put("model_name", Build.MODEL.toString());
            obj.put("platform", Constants.PLATFORM);
            obj.put("os_version", Constants.OS_VERSION);
//            obj.put("device_id", "af0e9d935156e875");
//            obj.put("model_name", "SM-N750L");
//            obj.put("platform", "1");
//            obj.put("os_version", "5.1.1");
            LogUtil.d("params : " + obj.toString());
            encodeParam = AES256Cipher.AES_Encode(obj.toString(), Constants.AES_KEY);
            LogUtil.d("encodeParam : " + encodeParam);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            JsonObject object = new JsonObject();
            object.addProperty("method", NetworkProperty.METHOD_DEVICE_CREATE);
            object.addProperty("params", encodeParam);
            Ion.with(this)
                    .load(NetworkProperty.HOST + "/jsonrpc")
                    .setTimeout(Constants.TIME_OUT)
                    .addHeader("Content-Type", "application/json")
                    .setJsonObjectBody(object)
                    .as(new TypeToken<JsonObject>() {
                    })
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject ret) {
                            if (e != null) {
                                LogUtil.e(e);
                            }

                            if (ret != null) {
                                try {
                                    LogUtil.d("ret : " + ret);
                                    JsonObject obj = ret;
                                    if (ret != null) {
                                        String token = ret.get("result").getAsJsonObject().get("auth_token").getAsString();
                                        LogUtil.d("token : " + token);
                                        mPrefsManager.authToken().put(token);
                                        getInstalledAppList();
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
