package com.howgame.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.howgame.android.R;
import com.howgame.android.activity.WebActivity_;
import com.howgame.android.activity.YouTubeFullscreenActivity;
import com.howgame.android.model.GameDetailModel;
import com.howgame.android.util.LogUtil;

import java.net.URLDecoder;

/**
 * Created by Administrator on 2016-09-21.
 */

public class DetailTopImageAdapter extends BaseAdapter {
    public DetailTopImageAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        if (!canGo())
            return;

        final GameDetailModel.ThumbnailInfo item = (GameDetailModel.ThumbnailInfo) mList.get(position);
        final ImageView ivThumbnail = mHolder.getAsImageView(R.id.ivThumbnail);
        final ImageView ivPlay = mHolder.getAsImageView(R.id.ivPlay);
        RelativeLayout rlView = mHolder.getAsRelativeLayout(R.id.rlView);
        if (item.getUrl() != null && item.getUrl().length() > 0) {
            Glide.with(mContext)
                    .load(URLDecoder.decode(item.getUrl()))
//                    .skipMemoryCache(true)
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(ivThumbnail);
        }
        if (item.getThumbnail_type() != null && item.getThumbnail_type().length() > 0) {
            if (item.getThumbnail_type().equals("3")) {
                ivPlay.setVisibility(View.VISIBLE);
                rlView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LogUtil.d("item.getVideo_url() : " + item.getVideo_url());
//                                WebActivity_.intent(mContext).mUrl(URLDecoder.decode(item.getVideo_url())).mPackageName(mPackageName).start();
                        if (item.getVideo_url().contains("youtube")) {
                            String code = null;
                            try {
                                Uri uri = Uri.parse(item.getVideo_url());
                                code = uri.getQueryParameter("v");
                                if (code == null) {
                                    LogUtil.d(uri.getLastPathSegment());
                                    LogUtil.d(uri.getHost());
                                    LogUtil.d(uri.getQuery());
                                    LogUtil.d(uri.getPath());
                                    LogUtil.d(uri.getPathSegments().toString());
                                    code = uri.getLastPathSegment();
                                }
                                if (code == null) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLDecoder.decode(item.getVideo_url())));
                                    mContext.startActivity(intent);
                                    return;
                                }
                            } catch (Exception e) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLDecoder.decode(item.getVideo_url())));
                                mContext.startActivity(intent);
                                return;
                            }
                            Intent intent = new Intent(mContext, YouTubeFullscreenActivity.class);
                            intent.putExtra("CODE", code);
                            mContext.startActivity(intent);
                        } else {
                            WebActivity_.intent(mContext).mUrl(URLDecoder.decode(item.getVideo_url())).mPackageName(null).start();
                        }
                    }
                });
            } else {
                ivPlay.setVisibility(View.GONE);
            }
        }
    }
}
