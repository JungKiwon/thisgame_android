package com.howgame.android.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.google.gson.reflect.TypeToken;
import com.howgame.android.R;
import com.howgame.android.adapter.BaseAdapter;
import com.howgame.android.adapter.DetailGuideAdapter;
import com.howgame.android.adapter.RelativeListAdapter;
import com.howgame.android.analytics.HowGameAnalyticsManager;
import com.howgame.android.api.BaseApi;
import com.howgame.android.api.GameDetailApi;
import com.howgame.android.model.GameDetailModel;
import com.howgame.android.property.Constants;
import com.howgame.android.property.NetworkProperty;
import com.howgame.android.util.HowGameUtil;
import com.howgame.android.util.LogUtil;
import com.howgame.android.view.RelativeListMarginDecoration;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016-08-23.
 */
@EActivity
public class DetailActivity extends BaseActivity {

    @Extra
    String mPackageName;

    @ViewById
    RelativeLayout rlTopBar;

    @ViewById
    LinearLayout llRelativeGame, llImageTop;

    @ViewById
    NestedScrollView svMain;

//    @ViewById
//    CirclePageIndicator viewPagerIndicator;

//    @ViewById
//    ViewPager viewPager;

    @ViewById
    ImageButton btnToolbarBack;
//    btnBack,

    @ViewById
    TextView toolbarTitle, tvGameInfoContent, tvReadMore, tvGameInfoContentMore;

    @ViewById
    CheckBox cbToolbarFavorite;
//    cbFavorite,

    @ViewById
    ObservableRecyclerView rvGuide, rvRelativeGame;
//            rvTopImage;

    @ViewById
    ImageView ivInfo;

    @ViewById
    TextView tvGameTitle, tvGamePublisher, tvGameRatingCount, tvHeaderDownload;

    @ViewById
    TextView tvGameStatusDownload, tvGameStatusStar, tvGameStatusCategory;

    @ViewById
    ImageView ivStar1, ivStar2, ivStar3, ivStar4, ivStar5;

    @ViewById
    HorizontalScrollView hsImageTop;


    //    private DetailTopImageAdapter mTopImageAdapter;
    private DetailGuideAdapter mGuideAdapter;
    private RelativeListAdapter mRelativeAdapter;

    private boolean isSendFavorite = false;

    private RelativeListMarginDecoration mDecoration;

    private boolean isAd = false;
//    private TopImageListMarginDecoration mTopImageDecoration;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        new HowGameAnalyticsManager(this).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_DETAIL);
        setLayout();
        initList();
        initToolbar();
        getGameInfo();
    }

    private void setLayout() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            RelativeLayout.LayoutParams paramsBack = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            paramsBack.setMargins(0, HowGameUtil.getPX(mActivity, 24), 0, 0);
//            RelativeLayout.LayoutParams paramsFavorite = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//            paramsFavorite.setMargins(0, HowGameUtil.getPX(mActivity, 24), 0, 0);
//            paramsFavorite.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//            RelativeLayout.LayoutParams paramsTopBar = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, HowGameUtil.getPX(mActivity, 50));
//            paramsTopBar.setMargins(0, HowGameUtil.getPX(mActivity, 24), 0, 0);
//            btnBack.setLayoutParams(paramsBack);
//            cbFavorite.setLayoutParams(paramsFavorite);
//            rlTopBar.setLayoutParams(paramsTopBar);
//        }

//        cbFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                sendFavorite();
//                if (isChecked) {
//                    cbToolbarFavorite.setChecked(true);
//                } else {
//                    cbToolbarFavorite.setChecked(false);
//                }
//            }
//        });


        cbToolbarFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sendFavorite();
                if (isChecked) {
                    cbToolbarFavorite.setChecked(true);
                } else {
                    cbToolbarFavorite.setChecked(false);
                }
            }
        });
    }

    private void initList() {
//        mTopImageAdapter = new DetailTopImageAdapter(mActivity);
//        LinearLayoutManager topImageManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
//        rvTopImage.setLayoutManager(topImageManager);
//        rvTopImage.setNestedScrollingEnabled(false);
//        rvTopImage.setHasFixedSize(true);
//        mTopImageAdapter.setResourceId(R.layout.item_top_image);
//        mTopImageAdapter.setRecyclerView(rvTopImage);
//        rvTopImage.setAdapter(mTopImageAdapter);
//        if (mTopImageDecoration == null) {
//            mTopImageDecoration = new TopImageListMarginDecoration(mActivity, 6);
//            rvTopImage.addItemDecoration(mTopImageDecoration);
//        }

        LinearLayoutManager guideLayoutManager = new LinearLayoutManager(mActivity);
        rvGuide.setLayoutManager(guideLayoutManager);
        rvGuide.setNestedScrollingEnabled(false);
        mGuideAdapter = new DetailGuideAdapter(mActivity);
        mGuideAdapter.setPackageName(mPackageName);
        mGuideAdapter.setResourceId(R.layout.list_row_guide);
        mGuideAdapter.setRecyclerView(rvGuide);
        rvGuide.setAdapter(mGuideAdapter);


        mRelativeAdapter = new RelativeListAdapter(mActivity);
        mRelativeAdapter.setPrefManager(mPrefsManager);
        GridLayoutManager relativeLayoutManager = new GridLayoutManager(mActivity, 2);
        relativeLayoutManager.setOrientation(GridLayout.VERTICAL);
        relativeLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int type = mRelativeAdapter.getItemViewType(position);
                if (type == BaseAdapter.VIEW_TYPE_HEADER) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        if (mDecoration == null) {
            mDecoration = new RelativeListMarginDecoration(mActivity, 8);
            rvRelativeGame.addItemDecoration(new RelativeListMarginDecoration(mActivity, 8));
        }
//        rvRelativeGame.removeItemDecoration(mDecoration);
        rvRelativeGame.setNestedScrollingEnabled(false);
        rvRelativeGame.setLayoutManager(relativeLayoutManager);
        mRelativeAdapter.setResourceId(R.layout.item_relative_list);
        mRelativeAdapter.setRecyclerView(rvRelativeGame);
        rvRelativeGame.setAdapter(mRelativeAdapter);

    }

//    private void initViewPager(ArrayList<GameDetailModel.ThumbnailInfo> dataSet) {
//        viewPager.setAdapter(new ThumbnailPagerAdapter(dataSet));
//        viewPagerIndicator.setViewPager(viewPager);
//    }


    /**
     * 툴바 초기화
     */
    private void initToolbar() {
//        svMain.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
////                LogUtil.d("setOnScrollChangeListener getScrollY  : " + HowGameUtil.getDP(mActivity, v.getScrollY()));
//                if (scrollY < oldScrollY) {
//
//                    if (HowGameUtil.getDP(mActivity, v.getScrollY()) <= 121) {
//                        if (rlTopBar.isShown()) {
//                            LogUtil.d("setOnScrollChangeListener up gone");
//                            rlTopBar.setVisibility(View.GONE);
////                            hideStatusBar(false);
//                        }
//                    }
//                } else {
//
//                    if (HowGameUtil.getDP(mActivity, v.getScrollY()) >= 121) {
//                        if (!rlTopBar.isShown()) {
//                            LogUtil.d("setOnScrollChangeListener down visible");
//                            rlTopBar.setVisibility(View.VISIBLE);
////                            hideStatusBar(true);
//                        }
//                    }
//                }
//            }
//        });
    }

    private void getGameInfo() {
        LogUtil.d("getGameInfo url : " + NetworkProperty.HOST + NetworkProperty.API_GAME_DETAIL);
        LogUtil.d("getGameInfo auth_token : " + mPrefsManager.authToken().get());
        LogUtil.d("getGameInfo package_name : " + mPackageName);
        show(getString(R.string.loading_relative_list));
        Ion.with(this)
                .load(NetworkProperty.HOST + NetworkProperty.API_GAME_DETAIL)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                .setBodyParameter("package_name", mPackageName)
                .as(new TypeToken<GameDetailApi>() {
                })
                .setCallback(new FutureCallback<GameDetailApi>() {
                    @Override
                    public void onCompleted(Exception e, GameDetailApi ret) {
                        dismiss();
                        if (e != null) {
                            LogUtil.e(e);
                        }

                        if (ret != null) {
                            try {
                                LogUtil.d("getGameInfo toJson : " + ret.getResult().toPrettyJson());
                                GameDetailModel model = ret.getResult();

                                ArrayList<GameDetailModel.ReviewInfo> infoList = model.getReview_info();
                                if (infoList != null && infoList.size() > 0) {
                                    mGuideAdapter.addAll(infoList);
                                    mGuideAdapter.notifyDataSetChanged();
                                }

                                if (model.getGame_info().getApp_info() != null && model.getGame_info().getApp_info().length() > 0) {
                                    tvGameInfoContent.setText(model.getGame_info().getApp_info());
                                    tvGameInfoContentMore.setText(model.getGame_info().getApp_info());
                                    if (tvGameInfoContentMore.getLineCount() > 4) {
                                        tvReadMore.setVisibility(View.VISIBLE);
                                    }
                                    tvGameInfoContentMore.setVisibility(View.GONE);
                                }

                                if (model.getGame_info().getTitle() != null && model.getGame_info().getTitle().length() > 0) {
                                    tvGameTitle.setText(model.getGame_info().getTitle());
                                    toolbarTitle.setText(model.getGame_info().getTitle());
                                    if (toolbarTitle.getLineCount() > 1) {
                                        toolbarTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                                    } else {
                                        toolbarTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                                    }
                                }

                                if (model.getGame_info().getIs_favorite() != null && model.getGame_info().getIs_favorite().length() > 0) {
                                    if ("Y".equals(model.getGame_info().getIs_favorite())) {
//                                        cbFavorite.setChecked(true);
                                        cbToolbarFavorite.setChecked(true);
                                    } else {
//                                        cbFavorite.setChecked(false);
                                        cbToolbarFavorite.setChecked(false);
                                    }
                                }
                                if ("Y".equals(model.getGame_info().getIs_ad())) {
                                    isAd = true;
                                } else {
                                    isAd = false;
                                }

                                if (model.getGame_info().getCompany() != null && model.getGame_info().getCompany().length() > 0) {
                                    tvGamePublisher.setText(model.getGame_info().getCompany());
                                }

                                if (model.getGame_info().getRating_count() != null && model.getGame_info().getRating_count().length() > 0) {
                                    String count = model.getGame_info().getRating_count().replace(",", "");
                                    tvGameRatingCount.setText(NumberFormat.getInstance().format(Integer.parseInt(count)));
                                }

                                tvGameStatusStar.setText(String.valueOf(model.getGame_info().getRating_value()));
                                if (model.getGame_info().getRating_value() >= 5.0f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_s);
                                } else if (model.getGame_info().getRating_value() >= 4.5f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_half_s);
                                } else if (model.getGame_info().getRating_value() >= 4.0f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                } else if (model.getGame_info().getRating_value() >= 3.5f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_half_s);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                } else if (model.getGame_info().getRating_value() >= 3.0f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                } else if (model.getGame_info().getRating_value() >= 2.5f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_half_s);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                } else if (model.getGame_info().getRating_value() >= 2.0f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                } else if (model.getGame_info().getRating_value() >= 1.5f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_half_s);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                } else if (model.getGame_info().getRating_value() >= 1.0f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                } else if (model.getGame_info().getRating_value() >= 0.5f) {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_half_s);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                } else {
                                    ivStar1.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar2.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar3.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar4.setBackgroundResource(R.drawable.ico_star_e);
                                    ivStar5.setBackgroundResource(R.drawable.ico_star_e);
                                }


                                if (model.getGame_info().getCategory() != null && model.getGame_info().getCategory().length() > 0) {
                                    tvGameStatusCategory.setText(model.getGame_info().getCategory());
                                }

                                if (model.getGame_info().getInstall_count() != null && model.getGame_info().getInstall_count().length() > 0) {
                                    tvGameStatusDownload.setText(HowGameUtil.countToString(mActivity, model.getGame_info().getInstall_count()));
                                }
                                ArrayList<GameDetailModel.ThumbnailInfo> thumbnailInfoList = model.getGame_info().getThumbnail_info();
                                if (thumbnailInfoList != null) {
                                    for (int i = 0; i < thumbnailInfoList.size(); i++) {
                                        if (thumbnailInfoList.get(i).getThumbnail_type().equals("1")) {
                                            Glide.with(mActivity)
                                                    .load(URLDecoder.decode(thumbnailInfoList.get(i).getUrl()))
                                                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                                    .into(ivInfo);
                                            thumbnailInfoList.remove(thumbnailInfoList.get(i));
                                        } else if (thumbnailInfoList.get(i).getThumbnail_type().equals("2")) {

                                        } else if (thumbnailInfoList.get(i).getThumbnail_type().equals("3")) {
                                            thumbnailInfoList.get(i).setVideo_url(model.getGame_info().getVideo_url());
                                        }
                                    }
                                    setTopImageView(thumbnailInfoList);
//
//                                    mTopImageAdapter.addAll(thumbnailInfoList);
//                                    mTopImageAdapter.notifyDataSetChanged();
                                    LogUtil.d("thumbnailInfoList : " + thumbnailInfoList.size());
                                } else {
                                    LogUtil.d("thumbnailInfoList is null");
                                }


                                ArrayList<GameDetailModel.SimilarInfo> similarInfoList = model.getGame_info().getSimilar_info();
                                if (similarInfoList != null && similarInfoList.size() > 0) {
                                    mRelativeAdapter.addAll(similarInfoList);
                                    mRelativeAdapter.notifyDataSetChanged();
                                    LogUtil.d("getSimilar_info : " + similarInfoList.size());
                                } else {
                                    LogUtil.d("similarInfoList is null");
                                    llRelativeGame.setVisibility(View.GONE);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                finish();
                            }
                        } else {
                            LogUtil.d("getGameInfo ret is null");
                        }
                    }
                });
    }


//    /**
//     * 포커스 테마 페이져 어댑터
//     */
//    public class ThumbnailPagerAdapter extends PagerAdapter {
//
//        ArrayList<GameDetailModel.ThumbnailInfo> mDataSet;
//        private LayoutInflater inflater;
//
//        public ThumbnailPagerAdapter(ArrayList<GameDetailModel.ThumbnailInfo> mDataSet) {
//            this.mDataSet = mDataSet;
//            this.inflater = LayoutInflater.from(mActivity);
//
//
//            if (mCountDownTimer != null)
//                mCountDownTimer.cancel();
//
//            mCountDownTimer = new CountDownTimer(3000, 3000) {
//                public void onTick(long millisUntilFinished) {
//                }
//
//                public void onFinish() {
//
//                    if (viewPager == null || !viewPager.isShown())
//                        return;
//
//                    int count = viewPager.getAdapter().getCount();
//                    int pos = viewPager.getCurrentItem();
//
//                    if (pos == count - 1) {
//                        viewPager.setCurrentItem(0);
//                    } else {
//                        viewPager.setCurrentItem(pos + 1, true);
//                    }
//                    start();
//                }
//            }.start();
//        }
//
//        @Override
//        public int getCount() {
//            return this.mDataSet.size();
//        }
//
//        @Override
//        public Object instantiateItem(ViewGroup container, int position) {
//            RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.game_thumbnail_item, container, false);
//            ImageView ivThumbnail = (ImageView) view.findViewById(R.id.ivThumbnail);
//            ImageView ivPlay = (ImageView) view.findViewById(R.id.ivPlay);
//            final GameDetailModel.ThumbnailInfo item = mDataSet.get(position);
//            if (item.getUrl() != null && item.getUrl().length() > 0) {
//                Glide.with(mContext)
//                        .load(URLDecoder.decode(item.getUrl()))
//                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                        .into(ivThumbnail);
//            }
//
//            if (item.getThumbnail_type() != null && item.getThumbnail_type().length() > 0) {
//                switch (item.getThumbnail_type()) {
//                    case "3": {
//                        ivPlay.setVisibility(View.VISIBLE);
//                        view.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                LogUtil.d("item.getVideo_url() : " + item.getVideo_url());
////                                WebActivity_.intent(mContext).mUrl(URLDecoder.decode(item.getVideo_url())).mPackageName(mPackageName).start();
//                                if (item.getVideo_url().contains("youtube")) {
//                                    String code = null;
//                                    try {
//                                        Uri uri = Uri.parse(item.getVideo_url());
//                                        code = uri.getQueryParameter("v");
//                                        if (code == null) {
//                                            LogUtil.d(uri.getLastPathSegment());
//                                            LogUtil.d(uri.getHost());
//                                            LogUtil.d(uri.getQuery());
//                                            LogUtil.d(uri.getPath());
//                                            LogUtil.d(uri.getPathSegments().toString());
//                                            code = uri.getLastPathSegment();
//                                        }
//                                        if (code == null) {
//                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLDecoder.decode(item.getVideo_url())));
//                                            startActivity(intent);
//                                            return;
//                                        }
//                                    } catch (Exception e) {
//                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLDecoder.decode(item.getVideo_url())));
//                                        startActivity(intent);
//                                        return;
//                                    }
//                                    Intent intent = new Intent(mContext, YouTubeFullscreenActivity.class);
//                                    intent.putExtra("CODE", code);
//                                    mContext.startActivity(intent);
//                                } else {
//                                    WebActivity_.intent(mContext).mUrl(URLDecoder.decode(item.getVideo_url())).mPackageName(mPackageName).start();
//                                }
////                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLDecoder.decode(item.getVideo_url())));
////                                startActivity(intent);
//                            }
//                        });
//                        break;
//                    }
//                    default: {
//                        ivPlay.setVisibility(View.GONE);
//                        break;
//                    }
//                }
//            }
//
//            container.addView(view);
//
//            return view;
//        }
//
//        @Override
//        public boolean isViewFromObject(View view, Object object) {
//            return view == object;
//        }
//
//        @Override
//        public void destroyItem(ViewGroup container, int position, Object object) {
//            ((ViewPager) container).removeView((View) object);
//        }
//
//        @Override
//        public int getItemPosition(Object object) {
//            return POSITION_NONE;
//        }
//    }

    @Click
    void tvHeaderDownload() {
        try {
            if (isAd) {
                new HowGameAnalyticsManager(mActivity).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_GOOGLE_PLAY + "_" + mPackageName + "_AD");
            } else {
                new HowGameAnalyticsManager(mActivity).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_GOOGLE_PLAY + "_" + mPackageName);
            }
            Intent intent = Intent.parseUri("market://details?id=" + mPackageName, Intent.URI_INTENT_SCHEME);
            if (intent != null) {
                startActivity(intent);
            }
        } catch (URISyntaxException e) {
            LogUtil.e(e);
        }
    }

    @Click
    void tvDownload() {
        try {
            if (isAd) {
                new HowGameAnalyticsManager(mActivity).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_GOOGLE_PLAY + "_" + mPackageName + "_AD");
            } else {
                new HowGameAnalyticsManager(mActivity).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_GOOGLE_PLAY + "_" + mPackageName);
            }
            Intent intent = Intent.parseUri("market://details?id=" + mPackageName, Intent.URI_INTENT_SCHEME);
            if (intent != null) {
                startActivity(intent);
            }
        } catch (URISyntaxException e) {
            LogUtil.e(e);
        }
    }

    @Click
    void btnToolbarBack() {
        finish();
    }

//    @Click
//    void btnBack() {
//        finish();
//    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        initList();
        initToolbar();
        getGameInfo();
    }

//    private void hideStatusBar(boolean isShow) {
//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
//            if (isShow) {
//                View decorView = getWindow().getDecorView();
//                // Hide Status Bar.
//                int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//                decorView.setSystemUiVisibility(uiOptions);
//                RelativeLayout.LayoutParams paramsTopBar = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, HowGameUtil.getPX(mActivity, 50));
//                paramsTopBar.setMargins(0, HowGameUtil.getPX(mActivity, 0), 0, 0);
//                rlTopBar.setLayoutParams(paramsTopBar);
//            } else {
//                View decorView = getWindow().getDecorView();
//                // Show Status Bar.
//                int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
//                decorView.setSystemUiVisibility(uiOptions);
//                RelativeLayout.LayoutParams paramsTopBar = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, HowGameUtil.getPX(mActivity, 50));
//                paramsTopBar.setMargins(0, HowGameUtil.getPX(mActivity, 24), 0, 0);
//                rlTopBar.setLayoutParams(paramsTopBar);
//            }
//        } else {
//            if (isShow) {
//                // Hide Status Bar.
//                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
//
//                RelativeLayout.LayoutParams paramsTopBar = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, HowGameUtil.getPX(mActivity, 50));
//                paramsTopBar.setMargins(0, HowGameUtil.getPX(mActivity, 0), 0, 0);
//                rlTopBar.setLayoutParams(paramsTopBar);
//            } else {
//                // Show Status Bar.
//                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//                RelativeLayout.LayoutParams paramsTopBar = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, HowGameUtil.getPX(mActivity, 50));
//                paramsTopBar.setMargins(0, HowGameUtil.getPX(mActivity, 24), 0, 0);
//                rlTopBar.setLayoutParams(paramsTopBar);
//            }
//        }
//    }

    private void sendFavorite() {
        if (!isSendFavorite) {
            LogUtil.d("sendFavorite url : " + NetworkProperty.HOST + NetworkProperty.API_GAME_TOGGLE_FAVORITE);
            LogUtil.d("sendFavorite auth_token : " + mPrefsManager.authToken().get());
            LogUtil.d("sendFavorite package_name : " + mPackageName);
            isSendFavorite = true;
            show(null);
            Ion.with(this)
                    .load(NetworkProperty.HOST + NetworkProperty.API_GAME_TOGGLE_FAVORITE)
                    .setTimeout(Constants.TIME_OUT)
                    .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                    .setBodyParameter("package_name", mPackageName)
                    .as(new TypeToken<BaseApi>() {
                    })
                    .setCallback(new FutureCallback<BaseApi>() {
                        @Override
                        public void onCompleted(Exception e, BaseApi ret) {
                            isSendFavorite = false;
                            dismiss();
                            if (e != null) {
                                LogUtil.e(e);
                            }

                            if (ret != null) {
                                try {
                                    LogUtil.d("sendFavorite msg : " + ret.getError_message());
                                } catch (Exception ex) {
                                }
                            }
                        }

                    });
        }
    }

    private void setTopImageView(ArrayList<GameDetailModel.ThumbnailInfo> thumbnailInfoList) {
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        int size = thumbnailInfoList.size();
        LinearLayout[] layoutList = new LinearLayout[size];
        for (int i = 0; i < size; i++) {
            final GameDetailModel.ThumbnailInfo item = thumbnailInfoList.get(i);
            layoutList[i] = (LinearLayout) inflater.inflate(R.layout.item_top_image, null);
            ImageView ivThumbnail = (ImageView) layoutList[i].findViewById(R.id.ivThumbnail);
            ImageView ivPlay = (ImageView) layoutList[i].findViewById(R.id.ivPlay);
            View vRight = (View) layoutList[i].findViewById(R.id.vRight);
            Glide.with(mActivity)
                    .load(item.getUrl())
                    .into(ivThumbnail);
            if ("3".equals(item.getThumbnail_type())) {
                ivPlay.setVisibility(View.VISIBLE);
                layoutList[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.getVideo_url().contains("youtube")) {
                            String code = null;
                            try {
                                Uri uri = Uri.parse(item.getVideo_url());
                                code = uri.getQueryParameter("v");
                                if (code == null) {
                                    LogUtil.d(uri.getLastPathSegment());
                                    LogUtil.d(uri.getHost());
                                    LogUtil.d(uri.getQuery());
                                    LogUtil.d(uri.getPath());
                                    LogUtil.d(uri.getPathSegments().toString());
                                    code = uri.getLastPathSegment();
                                }
                                if (code == null) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLDecoder.decode(item.getVideo_url())));
                                    mContext.startActivity(intent);
                                    return;
                                }
                            } catch (Exception e) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLDecoder.decode(item.getVideo_url())));
                                mContext.startActivity(intent);
                                return;
                            }
                            Intent intent = new Intent(mContext, YouTubeFullscreenActivity.class);
                            intent.putExtra("CODE", code);
                            mContext.startActivity(intent);
                        } else {
                            WebActivity_.intent(mContext).mUrl(URLDecoder.decode(item.getVideo_url())).mPackageName(null).start();
                        }
                    }
                });
            }
            if (i == size - 1) {
                vRight.setVisibility(View.GONE);
            } else {
                vRight.setVisibility(View.VISIBLE);
            }
            llImageTop.addView(layoutList[i]);
        }

    }

    @Click
    void tvReadMore() {
        tvReadMore.setVisibility(View.GONE);
        tvGameInfoContent.setVisibility(View.GONE);
        tvGameInfoContentMore.setVisibility(View.VISIBLE);

    }
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (mCountDownTimer != null) {
//            mCountDownTimer.start();
//        }
//    }

}
