package com.howgame.android.view;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import com.howgame.android.adapter.DetailTopImageAdapter;
import com.howgame.android.adapter.RelativeListAdapter;

/**
 * RecyclerView Decoration
 */
public class TopImageListMarginDecoration extends RecyclerView.ItemDecoration {
    private int margin;

    public TopImageListMarginDecoration(Context context, int margin) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        this.margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, margin, metrics);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        DetailTopImageAdapter adapter = (DetailTopImageAdapter) parent.getAdapter();
        if (adapter.getList().size() - 1 != parent.getChildAdapterPosition(view)) {
            outRect.set(0, 0, margin, 0);
        }

    }
}
