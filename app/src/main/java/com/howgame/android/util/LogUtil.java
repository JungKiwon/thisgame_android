package com.howgame.android.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 로그 유틸
 */
public class LogUtil {
    public static final boolean ENABLE = true;
//            public static final boolean ENABLE = false;
//        public static final boolean NETWORK = true;
    public static final boolean NETWORK = false;
    public static final boolean TOAST = true;
    public static final boolean EXCEPTION = true;
//    public static final boolean EXCEPTION = false;

    public static final String EXCEPTION_PREFIX = "[exception]";

    private static final String TAG = "[superfeed]";


    /**
     * 디버그 로그 출력
     *
     * @param text
     */
    public static void d(String text) {
        if (LogUtil.ENABLE) {
            Throwable t = new Throwable();
            StackTraceElement[] elements = t.getStackTrace();

            String callerClassName = elements[1].getClassName();
            String callerMethodName = elements[1].getMethodName();

            Log.d(TAG, "[" + callerClassName + ", " + callerMethodName + "] " + text);
        }
    }

    /**
     * 익셉션 로그 출력
     *
     * @param text
     */
    public static void e(String text) {
        if (LogUtil.ENABLE && LogUtil.EXCEPTION) {
            Throwable t = new Throwable();
            StackTraceElement[] elements = t.getStackTrace();

            String callerClassName = elements[1].getClassName();
            String callerMethodName = elements[1].getMethodName();

            Log.e(TAG, "[" + callerClassName + ", " + callerMethodName + "] " + EXCEPTION_PREFIX + text);
        }
    }

    /**
     * 익셉션 로그 출력
     *
     * @param e
     */
    public static void e(Exception e) {
        if (LogUtil.ENABLE && LogUtil.EXCEPTION) {
            if (e != null) {

                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                String exceptionAsString = sw.toString();

                Log.e(TAG, exceptionAsString);
            } else {
                Log.e(TAG, "!!! e == null !!!");
            }
        }
    }

    /**
     * 토스트 출력
     *
     * @param c
     * @param text
     */
    public static void t(Context c, String text) {
        Toast.makeText(c, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * 토스트 출력
     *
     * @param c
     * @param text
     */
    public static void tl(Context c, String text) {
        Toast.makeText(c, text, Toast.LENGTH_LONG).show();
    }
}
