package com.howgame.android.api;

/**
 * Created by Administrator on 2016-09-20.
 */
public class BaseApi {
    private int error_code;
    private String error_message;
    private String api_excute_time;

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public String getApi_excute_time() {
        return api_excute_time;
    }

    public void setApi_excute_time(String api_excute_time) {
        this.api_excute_time = api_excute_time;
    }
}
