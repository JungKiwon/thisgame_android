package com.howgame.android.manager;

import android.content.Context;

import com.howgame.android.activity.SplashActivity_;

import org.greenrobot.eventbus.EventBus;


/**
 * Created by Administrator on 2016-09-23.
 */

public class EventBusManager {

    /**
     * EventBus 등록 여부 확인
     *
     * @param object
     * @return
     */
    public static boolean isRegistered(Object object) {
        return EventBus.getDefault().isRegistered(object);
    }


    /**
     * EventBus 등록
     *
     * @param object { parameter_description }
     */
    public static void register(Object object) {
        EventBus.getDefault().register(object);
    }

    /**
     * EventBus 해제
     *
     * @param object
     */
    public static void unregister(Object object) {
        EventBus.getDefault().unregister(object);
    }

    /**
     * EventBus 발행
     *
     * @param object { parameter_description }
     */
    public static void post(Object object) {
        EventBus.getDefault().post(object);
    }

    /**
     * Activity 모두 종료 후 재실행
     *
     * @param context
     */
    public static void restart(Context context) {
        SplashActivity_.intent(context).start();
    }

    public static void getRecommendList() {

    }

}
