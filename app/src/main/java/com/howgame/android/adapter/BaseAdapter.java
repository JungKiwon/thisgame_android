package com.howgame.android.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.howgame.android.property.PrefsManager_;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-08-11.
 */
public class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // layout type
    public static final int VIEW_TYPE_1 = 1;
    public static final int VIEW_TYPE_2 = 2;
    public static final int VIEW_TYPE_3 = 3;
    public static final int VIEW_TYPE_4 = 4;
    public static final int VIEW_TYPE_HEADER = 5;
    public static final int VIEW_TYPE_FOOTER = 6;


    protected int[] mResourceIds = {-1, -1, -1, -1}; // 여러 뷰 타입일 경우 사용
    protected int mHeaderResourceId = -1; // 헤더 리소스
    protected int mFooterResourceId = -1; // 푸터 리소스

    protected Context mContext;
    protected ProgressDialog mProgressDlg;
    protected LayoutInflater mInflater;
    protected View.OnClickListener mOnClickListener;
    protected RecyclerViewHolder mHolder;

    // View type
    protected int mType;

    protected RecyclerView mRecyclerView;
    protected ArrayList<Object> mList;
    protected OnLoadMore mOnLoadMore;

    Object mHeaderData;
    Object mFooterData;

    protected PrefsManager_ mPrefsManager;


    public interface OnLoadMore {
        void onLoadMore();
    }

    /**
     * 추가 데이터 로딩 콜백 설정
     *
     * @param callback
     */
    public void setOnLoadMore(OnLoadMore callback) {
        mOnLoadMore = callback;
    }

    /**
     * RecyclerView 레퍼런스 저장
     *
     * @param recyclerView
     */
    public void setRecyclerView(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
    }

    /**
     * 헤더 리소스 설정
     *
     * @param resId
     */
    public void setHeaderResource(int resId) {
        mHeaderResourceId = resId;
    }


    /**
     * 푸터 리소스 설정
     *
     * @param resId
     */
    public void setFooterResource(int resId) {
        mFooterResourceId = resId;
    }

    /**
     * 헤더 제거
     */
    public void clearHeader() {
        mHeaderData = null;
    }

    /**
     * 헤더 존재 여부 확인
     *
     * @return
     */
    public boolean hasHeader() {
        return mHeaderResourceId > -1 && mHeaderData != null;
    }

    /**
     * 푸터 제거
     */
    public void clearFooter() {
        mFooterData = null;
    }

    /**
     * 푸터 존재 여부 확인
     *
     * @return { description_of_the_return_value }
     */
    public boolean hasFooter() {
        return mFooterResourceId > -1 && mFooterData != null;
    }

    /**
     * 헤더인지 체크
     *
     * @param position
     * @return
     */
    public boolean isHeader(int position) {
        if (mList == null)
            return false;

        return hasHeader() && position == 0;
    }

    /**
     * 푸터인지 체크
     *
     * @param position
     * @return
     */
    public boolean isFooter(int position) {

        if (mList == null)
            return false;

        return hasFooter() && (mList.size() - 1 == position);
    }


    public BaseAdapter(Context context) {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mList = new ArrayList();
    }

    /**
     * 아이템 클릭 리스너 설정
     *
     * @param listener
     */
    public void setOnClickListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }


    /**
     * 멀티 뷰 타입일 때 리소스 세팅
     *
     * @param ids
     */
    public void setResourceIds(int[] ids) {
        for (int i = 0; i < ids.length; i++)
            mResourceIds[i] = ids[i];
    }


    /**
     * 단일 뷰 타입일 때 리소스 세팅
     *
     * @param resourceId
     */
    public void setResourceId(int resourceId) {
        mResourceIds[0] = resourceId;
    }

    /**
     * 아이템 추가
     *
     * @param item
     */
    public void add(Object item) {
        if (mList == null)
            mList = new ArrayList();

        if (hasFooter() && mList.size() > 0) {
            mFooterData = mList.get(mList.size() - 1);
            mList.remove(mFooterData);
        }

        mList.add(item);

        if (hasFooter())
            mList.add(mFooterData);

        notifyDataSetChanged();
    }

    /**
     * 헤더 추가
     *
     * @param header
     */
    public void addHeader(Object header) {
        mHeaderData = header;
        getList().add(0, mHeaderData);
        notifyDataSetChanged();
    }

    /**
     * 푸터 추가
     *
     * @param footer
     */
    public void addFooter(Object footer) {
        mFooterData = footer;
        getList().add(footer);
        notifyDataSetChanged();
    }

    /**
     * 아이템 추가
     *
     * @param list
     */
    public void addAll(ArrayList list) {
        if (mList == null)
            mList = new ArrayList();

        if (hasFooter() && mList.size() > 0) {
            mFooterData = mList.get(mList.size() - 1);
            mList.remove(mFooterData);
        }

        mList.addAll(list);

        if (hasFooter()) {
            mList.add(mFooterData);
        }

        notifyDataSetChanged();
    }

    /**
     * 헤더 데이터 리턴
     *
     * @return
     */
    public Object getHeaderData() {
        return mHeaderData;
    }

    /**
     * 푸터 데이터 리턴
     *
     * @return
     */
    public Object getFooterData() {
        return mFooterData;
    }

    /**
     * 데이터 리스트 설정
     *
     * @param list
     */
    public void setList(ArrayList list) {

        // 헤더, 푸터 처리
        if (mList != null && mList.size() > 0) {

            int size = mList.size();

            if (hasHeader())
                mHeaderData = mList.get(0);

            if (hasFooter())
                mFooterData = mList.get(size - 1);
        }

        if (list == null)
            mList = new ArrayList();
        else
            mList = list;

        if (hasHeader())
            mList.add(0, mHeaderData);

        if (hasFooter())
            mList.add(mFooterData);

        notifyDataSetChanged();
    }

    /**
     * 데이터 리스트 리턴
     *
     * @return
     */
    public ArrayList getList() {
        if (mList == null)
            mList = new ArrayList();

        return mList;
    }

    /**
     * 데이터 리턴
     *
     * @param index
     * @return
     */
    public Object getAt(int index) {
        if (mList == null || index < 0 || mList.size() <= index) {
            return null;
        }
        return mList.get(index);
    }

    /**
     * 데이터 지우기
     * <p/>
     * 헤더와 푸터는 임시로 저장해둠
     */
    public void clear() {
        if (mList != null && mList.size() > 0) {

            if (hasHeader())
                mHeaderData = mList.get(0);

            if (hasFooter())
                mFooterData = mList.get(mList.size() - 1);

            mList.clear();
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {

        if (isHeader(position))
            return VIEW_TYPE_HEADER;
        else if (isFooter(position))
            return VIEW_TYPE_FOOTER;

        return VIEW_TYPE_1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_1)
            return new RecyclerViewHolder(mInflater.inflate(mResourceIds[0], parent, false));
        else if (viewType == VIEW_TYPE_2)
            return new RecyclerViewHolder(mInflater.inflate(mResourceIds[1], parent, false));
        else if (viewType == VIEW_TYPE_3)
            return new RecyclerViewHolder(mInflater.inflate(mResourceIds[2], parent, false));
        else if (viewType == VIEW_TYPE_4)
            return new RecyclerViewHolder(mInflater.inflate(mResourceIds[3], parent, false));
        else if (viewType == VIEW_TYPE_HEADER)
            return new RecyclerViewHolder(mInflater.inflate(mHeaderResourceId, parent, false));
        else if (viewType == VIEW_TYPE_FOOTER)
            return new RecyclerViewHolder(mInflater.inflate(mFooterResourceId, parent, false));
        else
            return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        mHolder = (RecyclerViewHolder) holder;
        mType = mHolder.getItemViewType();

        if (!canGo())
            return;

        // 추가 로딩 처리
        if ((position >= getItemCount() - 1)) {
            if (mOnLoadMore != null) {
                mOnLoadMore.onLoadMore();
            }
        }
    }

    /**
     * 표시 가능한 아이템 유무 확인
     *
     * @return
     */
    public boolean canGo() {
        return getItemCount() > 0;
    }

    /**
     * 표시 가능한 아이템 유무 확인
     *
     * @return
     */
    public boolean canGo(int position) {
        return getItemCount() > position;
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    public void setPrefManager(PrefsManager_ manager) {
        mPrefsManager = manager;
    }
}
