package com.howgame.android;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.howgame.android.service.HowGameService_;

import org.androidannotations.annotations.EApplication;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Administrator on 2016-08-11.
 */
@EApplication
public class HowGameApplication extends Application {
    Intent mServiceIntent;
    private HowGameService_ mService;

    @Override
    public void onCreate() {
        super.onCreate();
        Context context = getApplicationContext();
        mService = new HowGameService_();
        mServiceIntent = new Intent(context, mService.getClass());
        if (!isMyServiceRunning(mService.getClass())) {
            startService(mServiceIntent);
        }
        Fabric.with(this, new Crashlytics());
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

}
