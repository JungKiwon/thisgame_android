package com.howgame.android.api;

import com.howgame.android.model.CreateModel;

/**
 * Created by Administrator on 2016-09-20.
 */
public class CreateApi extends BaseApi {

    private CreateModel result;

    public CreateModel getResult() {
        return result;
    }

    public void setResult(CreateModel result) {
        this.result = result;
    }
}
