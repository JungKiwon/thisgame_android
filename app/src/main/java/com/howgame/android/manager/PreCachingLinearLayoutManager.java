package com.howgame.android.manager;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * RecyclerView LinearLayoutManager
 */
public class PreCachingLinearLayoutManager extends LinearLayoutManager {

    private static final int DEFAULT_EXTRA_LAYOUT_SPACE = 600;
    private int mExtraLayoutSpace = -1;
    private Context mContext;

    public PreCachingLinearLayoutManager(Context context) {
        super(context);
    }

    public void setExtraLayoutSpace(int extraLayoutSpace) {
        mExtraLayoutSpace = extraLayoutSpace;
    }

    @Override
    protected int getExtraLayoutSpace(RecyclerView.State state) {
        if (mExtraLayoutSpace > 0) {
            return (int) (mExtraLayoutSpace / 2f);
        }
        return DEFAULT_EXTRA_LAYOUT_SPACE;
    }

}
