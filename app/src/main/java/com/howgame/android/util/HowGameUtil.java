package com.howgame.android.util;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.howgame.android.R;

/**
 * Created by Administrator on 2016-09-21.
 */

public class HowGameUtil {

    /**
     * @param dp
     * @return px
     */

    public static int getPX(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    public static int getDP(Context context, int px) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (px / (1.5f * scale));
    }


    public static String countToString(Context context, String count) {
        LogUtil.d("countToString : " + count);
        String trimCount = count.replace(",", "").trim();
        long numCount = Long.parseLong(trimCount);
        if (numCount >= 5000000000l) {
            count = context.getResources().getString(R.string.five_bil);
        } else if (numCount >= 1000000000) {
            count = context.getResources().getString(R.string.one_bil);
        } else if (numCount >= 500000000) {
            count = context.getResources().getString(R.string.five_hundred_mil);
        } else if (numCount >= 100000000) {
            count = context.getResources().getString(R.string.one_hundred_mil);
        } else if (numCount >= 50000000) {
            count = context.getResources().getString(R.string.fifty_mil);
        } else if (numCount >= 10000000) {
            count = context.getResources().getString(R.string.ten_mil);
        } else if (numCount >= 5000000) {
            count = context.getResources().getString(R.string.five_mil);
        } else if (numCount >= 1000000) {
            count = context.getResources().getString(R.string.one_mil);
        } else if (numCount >= 500000) {
            count = context.getResources().getString(R.string.five_hundred_thousand);
        } else if (numCount >= 100000) {
            count = context.getResources().getString(R.string.one_hundred_thousand);
        } else if (numCount >= 50000) {
            count = context.getResources().getString(R.string.fifty_thousand);
        } else if (numCount >= 10000) {
            count = context.getResources().getString(R.string.ten_thousand);
        }
        return count;
    }

    public static boolean isEmpty(final CharSequence cs) {
        return cs == null || cs.length() == 0;
    }

    public static String defaultString(final String str) {
        return str == null ? "" : str;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.heightPixels;
    }

}
