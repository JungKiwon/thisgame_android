package com.howgame.android.manager;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.bumptech.glide.module.GlideModule;

/**
 * Created by Administrator on 2016-08-02.
 */
public class HowGameGlideModule implements GlideModule {
    //    private final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
//    private final int cacheSize = maxMemory / 8;
    private final int DISK_CACHE_SIZE = 1024 * 1024 * 250;

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
//        String downloadDirectoryPath = Environment.getDownloadCacheDirectory().getPath();
        MemorySizeCalculator calculator = new MemorySizeCalculator(context);
        int defaultMemoryCacheSize = calculator.getMemoryCacheSize();
        int defaultBitmapPoolSize = calculator.getBitmapPoolSize();

        int customMemoryCacheSize = (int) (1.2 * defaultMemoryCacheSize);
        int customBitmapPoolSize = (int) (1.2 * defaultBitmapPoolSize);
        builder
                .setMemoryCache(new LruResourceCache(customMemoryCacheSize))
                .setBitmapPool(new LruBitmapPool(customBitmapPoolSize))
//                .setDiskCache(new ExternalCacheDiskCacheFactory(context, "cache", DISK_CACHE_SIZE));
//                .setDiskCache(new DiskLruCacheFactory(downloadDirectoryPath, DISK_CACHE_SIZE));
                .setDiskCache(new InternalCacheDiskCacheFactory(context, DISK_CACHE_SIZE));
    }

    @Override
    public void registerComponents(Context context, Glide glide) {
        // register ModelLoaders here.
    }
}
