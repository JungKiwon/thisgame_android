package com.howgame.android.api;

import com.howgame.android.model.TopListsModel;

/**
 * Created by Administrator on 2016-09-20.
 */
public class TopListsApi extends BaseApi {

    private TopListsModel result;

    public TopListsModel getResult() {
        return result;
    }

    public void setResult(TopListsModel result) {
        this.result = result;
    }
}
