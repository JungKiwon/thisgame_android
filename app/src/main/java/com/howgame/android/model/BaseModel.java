package com.howgame.android.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Administrator on 2016-09-20.
 */
public class BaseModel {

    public static <T> String toPrettyJson(Object obj, Class<T> type) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(obj, type);
    }

    /**
     * JSON 문자열로부터 오브젝트 생성
     *
     * @param type
     * @param json
     * @return
     */
    public static <T> T fromJSON(Class<T> type, String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, type);
    }

    /**
     * 오브젝트를 JSON 문자열로 변환
     *
     * @return
     */
    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    /**
     * 정렬된 JSON 형태를 생성
     *
     * @param obj
     * @param type
     * @return
     */
    public String toPrettyJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }
}
