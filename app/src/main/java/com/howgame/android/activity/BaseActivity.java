package com.howgame.android.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.howgame.android.manager.HowGameFragmentManager;
import com.howgame.android.property.PrefsManager_;
import com.howgame.android.util.HowGameUtil;
import com.howgame.android.util.LogUtil;
import com.howgame.android.view.HowGameProgressDialog;
import com.howgame.android.view.LoadingDialog;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.sharedpreferences.Pref;

/**
 * Created by Administrator on 2016-08-11.
 */
@EActivity
public class BaseActivity extends AppCompatActivity {
    protected Context mContext;
    protected Activity mActivity;
    protected HowGameProgressDialog mProgressDlg;
    protected HowGameFragmentManager mFragmentManager; // 프래그먼트 매니저
    @Pref
    protected PrefsManager_ mPrefsManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        mActivity = this;
    }

    /**
     * 로딩 다이얼로그 종료
     */
    public void dismiss() {

//        mLoadingManager.dismiss();
        try {
            if (mProgressDlg != null && mProgressDlg.isShowing()) {
                mProgressDlg.dismiss();
                mProgressDlg = null;
            }
        } catch (Exception e) {
            LogUtil.e(e);
        }
    }

    /**
     * 로딩 다이얼로그 실행
     *
     * @param msg { parameter_description }
     */
    public void show(String msg) {

        if (mActivity == null || mActivity.isFinishing())
            return;


//        mLoadingManager.show(msg);
        if (HowGameUtil.isEmpty(msg)) {
            msg = "로드중...";
        }

        if (mProgressDlg == null || !mProgressDlg.isShowing()) {
            try {
                mProgressDlg = new HowGameProgressDialog(mContext, msg);
                mProgressDlg.show();
            } catch (Exception e) {
                LogUtil.e(e);
            }
        }
    }

}
