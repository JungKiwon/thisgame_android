package com.howgame.android.analytics;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Administrator on 2016-09-23.
 */

public class HowGameAnalyticsManager {

    private static FirebaseAnalytics mAnalytics;

    public HowGameAnalyticsManager(Context context) {
        mAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public static void sendAnalytics(String name) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, name);
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "SCREEN");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
            mAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
        } catch (Exception e) {

        }
    }

}
