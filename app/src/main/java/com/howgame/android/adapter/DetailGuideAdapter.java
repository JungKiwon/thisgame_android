package com.howgame.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.howgame.android.R;
import com.howgame.android.activity.WebActivity_;
import com.howgame.android.model.GameDetailModel;
import com.howgame.android.util.LogUtil;

import java.net.URLDecoder;

/**
 * Created by Administrator on 2016-09-21.
 */

public class DetailGuideAdapter extends BaseAdapter {
    private String mPackageName;

    public DetailGuideAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        if (!canGo())
            return;

        final GameDetailModel.ReviewInfo item = (GameDetailModel.ReviewInfo) mList.get(position);
        TextView tvTitle = mHolder.getAsTextView(R.id.tvTitle);
        if (item.getReview_title() != null && item.getReview_title().length() > 0) {
            tvTitle.setText(item.getReview_title());
            LogUtil.d(position + " title : " + item.getReview_title());
        }
        RelativeLayout rlRow = mHolder.getAsRelativeLayout(R.id.rlRow);
        rlRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getReview_url() != null && item.getReview_url().length() > 0) {
                    WebActivity_.intent(mContext).mUrl(URLDecoder.decode(item.getReview_url())).mPackageName(mPackageName).mTitle(item.getReview_title()).start();
                }
            }
        });

    }

    public void setPackageName(String packageName) {
        mPackageName = packageName;
    }
}
