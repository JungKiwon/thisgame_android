package com.howgame.android.model;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-09-20.
 */
public class GameDetailModel extends BaseModel {
    private ArrayList<ReviewInfo> review_info;
    private GameInfo game_info;//	Object	O	게임 정보
    private String market_url;

    public ArrayList<ReviewInfo> getReview_info() {
        return review_info;
    }

    public void setReview_info(ArrayList<ReviewInfo> review_info) {
        this.review_info = review_info;
    }

    public GameInfo getGame_info() {
        return game_info;
    }

    public void setGame_info(GameInfo game_info) {
        this.game_info = game_info;
    }


    public class ReviewInfo {
        private String review_title;
        private String review_url;

        public String getReview_title() {
            return review_title;
        }

        public void setReview_title(String review_title) {
            this.review_title = review_title;
        }

        public String getReview_url() {
            return review_url;
        }

        public void setReview_url(String review_url) {
            this.review_url = review_url;
        }
    }

    public class GameInfo {
        private int seq;//O	게임 고유 정보
        private String package_name;//	string	O	패키지명
        private String title;//	string	O	게임명
        private float rating_value;//		O	평점
        private String rating_count;//	string	O	평점 참여수
        private String company;//	string	O	제작사
        private String is_active;//	string	O	활성여부
        private String category;//	string	O	장르
        private String video_url;//	string	X	비디오 소개 영상 URL
        private String app_info;//	string	O	게임 소개 정보
        private String is_ad;//	string 	O	광고 여부
        private String install_count;//	string	O	설치수
        private String is_favorite;//	string	O	좋아요 활성 여부
        private ArrayList<ThumbnailInfo> thumbnail_info;//	Object	O	스크린샷 및 로고 정보
        private ArrayList<SimilarInfo> similar_info;//	Object	O	연관앱 정보

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }

        public String getPackage_name() {
            return package_name;
        }

        public void setPackage_name(String package_name) {
            this.package_name = package_name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public float getRating_value() {
            return rating_value;
        }

        public void setRating_value(float rating_value) {
            this.rating_value = rating_value;
        }

        public String getRating_count() {
            return rating_count;
        }

        public void setRating_count(String rating_count) {
            this.rating_count = rating_count;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getIs_active() {
            return is_active;
        }

        public void setIs_active(String is_active) {
            this.is_active = is_active;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }

        public String getApp_info() {
            return app_info;
        }

        public void setApp_info(String app_info) {
            this.app_info = app_info;
        }

        public String getIs_ad() {
            return is_ad;
        }

        public void setIs_ad(String is_ad) {
            this.is_ad = is_ad;
        }

        public String getInstall_count() {
            return install_count;
        }

        public void setInstall_count(String install_count) {
            this.install_count = install_count;
        }

        public String getIs_favorite() {
            return is_favorite;
        }

        public void setIs_favorite(String is_favorite) {
            this.is_favorite = is_favorite;
        }

        public ArrayList<ThumbnailInfo> getThumbnail_info() {
            return thumbnail_info;
        }

        public void setThumbnail_info(ArrayList<ThumbnailInfo> thumbnail_info) {
            this.thumbnail_info = thumbnail_info;
        }

        public ArrayList<SimilarInfo> getSimilar_info() {
            return similar_info;
        }

        public void setSimilar_info(ArrayList<SimilarInfo> similar_info) {
            this.similar_info = similar_info;
        }
    }

    public class ThumbnailInfo {
        private String thumbnail_type;//	string	O	썸네일 타입	1: 로고 ,2: 스크린샷 ,3: 동영상
        private String url;//	string	O	썸네일 URL
        private String video_url;//	string	O	썸네일 URL

        public String getThumbnail_type() {
            return thumbnail_type;
        }

        public void setThumbnail_type(String thumbnail_type) {
            this.thumbnail_type = thumbnail_type;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getVideo_url() {
            return video_url;
        }

        public void setVideo_url(String video_url) {
            this.video_url = video_url;
        }
    }

    public class SimilarInfo {

        private String similar_package_name;//	string	O	연관앱 패키지명
        private String title;//	string	O	연관앱 게임명
        private String company;//	string	O	연관앱 제작사
        private float rating_value;//	float	O	연관앱 평정
        private String rating_count;//	string	O	연관앱 평정 참여자수
        private String thumbnail_url;//	string	O
        private String category;//	string	O	연관앱 패키지명

        public String getSimilar_package_name() {
            return similar_package_name;
        }

        public void setSimilar_package_name(String similar_package_name) {
            this.similar_package_name = similar_package_name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public float getRating_value() {
            return rating_value;
        }

        public void setRating_value(float rating_value) {
            this.rating_value = rating_value;
        }

        public String getRating_count() {
            return rating_count;
        }

        public void setRating_count(String rating_count) {
            this.rating_count = rating_count;
        }

        public String getThumbnail_url() {
            return thumbnail_url;
        }

        public void setThumbnail_url(String thumbnail_url) {
            this.thumbnail_url = thumbnail_url;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }
    }

    public String getMarket_url() {
        return market_url;
    }

    public void setMarket_url(String market_url) {
        this.market_url = market_url;
    }
}
