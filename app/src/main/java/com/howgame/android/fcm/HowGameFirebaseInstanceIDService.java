/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.howgame.android.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.reflect.TypeToken;
import com.howgame.android.api.BaseApi;
import com.howgame.android.property.Constants;
import com.howgame.android.property.NetworkProperty;
import com.howgame.android.property.PrefsManager;
import com.howgame.android.property.PrefsManager_;
import com.howgame.android.util.LogUtil;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.androidannotations.annotations.EIntentService;

public class HowGameFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "HowGameFirebaseInstanceIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        LogUtil.d("Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        PrefsManager_ pref = new PrefsManager_(getApplicationContext());
        LogUtil.d("sendFcmToken url : " + NetworkProperty.HOST + NetworkProperty.API_DEVICE_UPDATE_PUSH_TOKEN);
        LogUtil.d("sendFcmToken auth_token : " + pref.authToken().get());
        LogUtil.d("sendFcmToken push_token : " + token);
        Ion.with(this)
                .load(NetworkProperty.HOST + NetworkProperty.API_DEVICE_UPDATE_PUSH_TOKEN)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", pref.authToken().get())
                .setBodyParameter("push_token", token)
                .as(new TypeToken<BaseApi>() {
                })
                .setCallback(new FutureCallback<BaseApi>() {
                    @Override
                    public void onCompleted(Exception e, BaseApi ret) {
                        if (e != null) {
                            LogUtil.e(e);
                        }

                        if (ret != null) {
                            LogUtil.d("sendFcmToken msg : " + ret.getError_message());
                        }
                    }
                });
    }
}