package com.howgame.android.property;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;


/**
 * SharedPreference 매니저
 */
@SharedPref(value = SharedPref.Scope.UNIQUE)
public interface PrefsManager {

    @DefaultString("")
    String authToken();

    @DefaultBoolean(true)
    boolean pushOn();


}
