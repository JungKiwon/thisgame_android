package com.howgame.android.api;

import com.howgame.android.model.GameDetailModel;

/**
 * Created by Administrator on 2016-09-20.
 */
public class GameDetailApi extends BaseApi {


    private GameDetailModel result;

    public GameDetailModel getResult() {
        return result;
    }

    public void setResult(GameDetailModel result) {
        this.result = result;
    }
}
