package com.howgame.android.adapter;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;


/**
 * RecyclerViewHolder
 */
public class RecyclerViewHolder extends RecyclerView.ViewHolder {

    private SparseArray<View> mHolder = new SparseArray<View>();
    private View mTopView;

    public RecyclerViewHolder(View v) {
        super(v);

        mTopView = v;
        deepScan((ViewGroup) v);
    }

    /**
     * 뷰 그룹 스캔 및 맵 세팅
     *
     * @param v
     */
    private void deepScan(ViewGroup v) {

        if (v.getChildCount() == 0)
            return;

        for (int i = 0; i < v.getChildCount(); ++i) {

            View nextChild = v.getChildAt(i);

            put(nextChild);

            if (nextChild instanceof ViewGroup)
                deepScan((ViewGroup) nextChild);
        }
    }

    /**
     * 최상단 뷰 리턴
     *
     * @return
     */
    public View getTopView() {
        return mTopView;
    }

    /**
     * 뷰 리턴
     *
     * @param id
     * @return
     */
    public <T extends View> T get(Integer id) {
        return (T) mHolder.get(id);
    }

    /**
     * 뷰 저장
     *
     * @param view
     */
    public void put(View view) {
        put(view, view.getId());
    }

    /**
     * 뷰 저장
     *
     * @param view
     * @param id
     */
    public void put(View view, Integer id) {

        if (id == -1)
            return;

        mHolder.put(id, view);
    }

    /**
     * 부모 뷰의 뷰 저장
     *
     * @param parent
     * @param id
     */
    public void putViewFromParent(View parent, Integer id) {
        View view = parent.findViewById(id);
        put(view, id);
    }

    /**
     * 뷰홀더 초기화
     */
    public void clear() {
        mHolder.clear();
    }

    public TextView getAsTextView(int id) {
        return (TextView) mHolder.get(id);
    }

    public ImageView getAsImageView(int id) {
        return (ImageView) mHolder.get(id);
    }

    public EditText getAsEditText(int id) {
        return (EditText) mHolder.get(id);
    }

    public Button getAsButton(int id) {
        return (Button) mHolder.get(id);
    }

    public ImageButton getAsImageButton(int id) {
        return (ImageButton) mHolder.get(id);
    }

    public CheckBox getAsCheckBox(int id) {
        return (CheckBox) mHolder.get(id);
    }

    public LinearLayout getAsLinearLayout(int id) {
        return (LinearLayout) mHolder.get(id);
    }

    public RelativeLayout getAsRelativeLayout(int id) {
        return (RelativeLayout) mHolder.get(id);
    }


    public ViewPager getAsViewPager(int id) {
        return (ViewPager) mHolder.get(id);
    }

    public CirclePageIndicator getAsCirclePageIndicator(int id) {
        return (CirclePageIndicator) mHolder.get(id);
    }

    public HorizontalScrollView getAsHorizontalScrollView(int id) {
        return (HorizontalScrollView) mHolder.get(id);
    }

    /**
     * 큰 이미지 리스트 상 하단
     */
    public View getAsView(int id) {
        return (View) mHolder.get(id);
    }


    public WebView getAsWebView(int id) {
        return (WebView) mHolder.get(id);
    }


    public FrameLayout getAsFrameLayout(int id) {
        return (FrameLayout) mHolder.get(id);
    }

}
