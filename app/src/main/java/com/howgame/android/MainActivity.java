package com.howgame.android;

import android.content.Intent;
import android.os.Bundle;

import com.google.gson.reflect.TypeToken;
import com.howgame.android.activity.BaseActivity;
import com.howgame.android.activity.DetailActivity_;
import com.howgame.android.analytics.HowGameAnalyticsManager;
import com.howgame.android.api.BaseApi;
import com.howgame.android.fragment.MainListFragment_;
import com.howgame.android.manager.HowGameFragmentManager;
import com.howgame.android.property.Constants;
import com.howgame.android.property.NetworkProperty;
import com.howgame.android.util.LogUtil;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.androidannotations.annotations.EActivity;

@EActivity
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        checkPushMsg();
        new HowGameAnalyticsManager(this).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_MAIN);

        initGame();

        if (mFragmentManager == null) {
            mFragmentManager = new HowGameFragmentManager(mActivity, R.id.llMain);
        }
        try {
            mFragmentManager.replace(getSupportFragmentManager(), MainListFragment_.builder().build(), false);
        } catch (Exception e) {
        }
    }

    private void initGame() {
        LogUtil.d("initGame url : " + NetworkProperty.HOST + NetworkProperty.API_GAME_INIT);
        LogUtil.d("initGame auth_token : " + mPrefsManager.authToken().get());
        show(getString(R.string.loading_init));
        Ion.with(this)
                .load(NetworkProperty.HOST + NetworkProperty.API_GAME_INIT)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                .as(new TypeToken<BaseApi>() {
                })
                .setCallback(new FutureCallback<BaseApi>() {
                    @Override
                    public void onCompleted(Exception e, BaseApi ret) {
                        dismiss();
                        if (e != null) {
                            LogUtil.e(e);
                        }

                        if (ret != null) {
                            LogUtil.d("initGame msg : " + ret.getError_message());
                        }
//                        DetailActivity_.intent(mContext).mPackageName("com.netmarble.chachacha2g").start();
                    }
                });
    }

    private void checkPushMsg() {
        Intent intent = getIntent();
        if (intent != null) {
            final String packageName = intent.getStringExtra("package_name");
            String hashKey = intent.getStringExtra("hash_key");
            if (packageName != null && packageName.length() > 0) {
                LogUtil.d("checkPushMsg url : " + NetworkProperty.HOST + NetworkProperty.API_STAT_PUSH_MARK_AS_READ);
                LogUtil.d("checkPushMsg hashKey : " + hashKey);
                show(getString(R.string.loading_init));
                Ion.with(this)
                        .load(NetworkProperty.HOST + NetworkProperty.API_STAT_PUSH_MARK_AS_READ)
                        .setTimeout(Constants.TIME_OUT)
                        .setBodyParameter("hash_key", hashKey)
                        .as(new TypeToken<BaseApi>() {
                        })
                        .setCallback(new FutureCallback<BaseApi>() {
                            @Override
                            public void onCompleted(Exception e, BaseApi ret) {
                                dismiss();
                                if (e != null) {
                                    LogUtil.e(e);
                                }

                                if (ret != null) {
                                    LogUtil.d("checkPushMsg msg : " + ret.getError_message());
                                    DetailActivity_.intent(mActivity).mPackageName(packageName).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
                                }
                            }
                        });
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        checkPushMsg();
        new HowGameAnalyticsManager(this).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_MAIN);
        initGame();
        if (mFragmentManager == null) {
            mFragmentManager = new HowGameFragmentManager(mActivity, R.id.llMain);
        }
        try {
            mFragmentManager.replace(getSupportFragmentManager(), MainListFragment_.builder().build(), false);
        } catch (Exception e) {
        }
    }
}
