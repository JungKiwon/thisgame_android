package com.howgame.android.property;

/**
 * Created by Administrator on 2016-09-20.
 */
public class NetworkProperty {
    //    public static final String HOST = "http://test.api.thisgame.co.kr";
    public static final String HOST = "http://api.thisgame.co.kr";
    public static final String API_DEVICE_CREATE = "/device/create";
    public static final String METHOD_DEVICE_CREATE = "device.create";
    public static final String API_DEVICE_UPDATE_PUSH_TOKEN = "/device/update_push_token";
    public static final String API_DEVICE_TOGGLE_USE_PUSH = "/device/toggle_use_push";


    public static final String API_INSTALL_ADD = "/install/add";
    public static final String METHOD_INSTALL_ADD = "install.add";


    public static final String API_GAME_INIT = "/game/init";
    public static final String API_GAME_DETAIL = "/game/detail";
    public static final String API_GAME_TOGGLE_FAVORITE = "/game/toggle_favorite";
    public static final String API_GAME_TOP_LISTS = "/game/top_lists";
    public static final String API_GAME_RECOMMEND_LISTS = "/game/recommend_lists";
    public static final String API_GAME_CLICK = "/game/click";

    public static final String API_STAT_PUSH_MARK_AS_READ = "/stat/push_mark_as_read";


}
