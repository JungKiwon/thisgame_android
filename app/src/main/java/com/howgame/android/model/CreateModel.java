package com.howgame.android.model;

/**
 * Created by Administrator on 2016-09-20.
 */
public class CreateModel extends BaseModel {

    private String auth_token;

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }
}
