package com.howgame.android.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.howgame.android.R;
import com.howgame.android.analytics.HowGameAnalyticsManager;
import com.howgame.android.property.Constants;
import com.howgame.android.util.HowGameUtil;
import com.howgame.android.util.LogUtil;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.net.URISyntaxException;

/**
 * Created by Administrator on 2016-08-23.
 */
@EActivity
public class WebActivity extends BaseActivity {
    @Extra
    String mUrl;

    @Extra
    String mTitle;

    @Extra
    String mPackageName;

    @ViewById
    LinearLayout llWebView;

    @ViewById
    ImageButton btnWebViewBack, btnWebViewDownload;

    @ViewById
    TextView tvWebViewTitle;

    @ViewById
    RelativeLayout rlHeader;

    private WebView webView;
    private String mOriginalUrl = "";
    private View mCustomView;
    private HowGameWebChromeClient mChromeClient;
    private WebChromeClient.CustomViewCallback customViewCallback;

    @ViewById
    FrameLayout customViewContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        mOriginalUrl = mUrl;
        LogUtil.d("url : " + mUrl);
        new HowGameAnalyticsManager(this).sendAnalytics(Constants.FIREBASE_ANALYTICS_ITEM_WEB);
        setWebView();
    }


    @Override
    public void onBackPressed() {
        if (inCustomView()) {
            hideCustomView();
            return;
        }

        if (webView != null && webView.canGoBack()) {
            LogUtil.d("onBackPressed can go back");
            try {
                LogUtil.d("onBackPressed webView.getOriginalUrl() : " + webView.getOriginalUrl());
                LogUtil.d("onBackPressed webView.getUrl() : " + webView.getUrl());
                if (!mOriginalUrl.equals(webView.getUrl())) {
                    LogUtil.d("onBackPressed url not equal can go back");
                    try {
                        webView.goBack();
                    } catch (Exception e) {
                        destroyWebView();
                    }
                } else {
                    LogUtil.d("onBackPressed url equal onBackPressed");
                    destroyWebView();
                }
            } catch (Exception e) {
                LogUtil.d("onBackPressed excaption onBackPressed");
                destroyWebView();
            }
        } else {
            LogUtil.d("onBackPressed can't go back");
            destroyWebView();
        }
    }

    private void setWebView() {
        webView = new WebView(mActivity);
        mChromeClient = new HowGameWebChromeClient();
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        if (llWebView.getChildCount() == 0) {
            llWebView.addView(webView);
        }

        tvWebViewTitle.setText(HowGameUtil.defaultString(mTitle));

        webView.setWebViewClient(new HowGameWebViewClient());
        webView.setWebChromeClient(mChromeClient);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);

        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.getSettings().setAppCacheEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webView.loadUrl(mUrl);

    }

    private class HowGameWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url != null && url.startsWith("intent://")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Intent existPackage = getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                    if (existPackage != null) {
                        startActivity(intent);
                    } else {
                        Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                        marketIntent.setData(Uri.parse("market://details?id=" + intent.getPackage()));
                        startActivity(marketIntent);
                    }
                    return true;
                } catch (Exception e) {
                    LogUtil.e(e);
                }
            } else if (url != null && url.startsWith("market://")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    if (intent != null) {
                        startActivity(intent);
                    }
                    return true;
                } catch (URISyntaxException e) {
                    LogUtil.e(e);
                }
            }
            return super.shouldOverrideUrlLoading(view, url);
        }
    }


    private class HowGameWebChromeClient extends WebChromeClient {
        private View mVideoProgressView;

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);

            if (HowGameUtil.isEmpty(mTitle)) {
                tvWebViewTitle.setText(HowGameUtil.defaultString(title));
            }
        }

        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            try {
                onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
            } catch (Exception e) {
            }
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }

            try {
                hideStatusBar(true);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                mCustomView = view;
                rlHeader.setVisibility(View.GONE);
                customViewContainer.setVisibility(View.VISIBLE);
                customViewContainer.addView(view);
                customViewCallback = callback;
            } catch (Exception e) {
            }
        }

        @Override
        public View getVideoLoadingProgressView() {
            try {
                if (mVideoProgressView == null) {
                    LayoutInflater inflater = LayoutInflater.from(mActivity);
                    mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
                }
            } catch (Exception e) {
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            try {
                hideStatusBar(false);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                rlHeader.setVisibility(View.VISIBLE);
                customViewContainer.setVisibility(View.GONE);

                // Hide the custom view.
                mCustomView.setVisibility(View.GONE);

                // Remove the custom view from its container.
                customViewContainer.removeView(mCustomView);
                customViewCallback.onCustomViewHidden();

                mCustomView = null;
            } catch (Exception e) {
            }
        }

        /**
         * 상단 로딩바 추가
         */
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
//            progressBar.setProgress(newProgress);
//            if (newProgress == 100) {
//                progressBar.setVisibility(View.GONE);
//            } else if (newProgress == 0) {
//                progressBar.setVisibility(View.VISIBLE);
//            }
        }
    }

    @Click
    void btnWebViewDownload() {
        try {
            Intent intent = Intent.parseUri("market://details?id=" + mPackageName, Intent.URI_INTENT_SCHEME);
            if (intent != null) {
                startActivity(intent);
            }
        } catch (URISyntaxException e) {
            LogUtil.e(e);
        }
    }

    private void destroyWebView() {
        try {
            if (webView != null) {
                webView.destroy();
                webView = null;
                llWebView.removeAllViews();
            }
            finish();
        } catch (Exception e) {
            finish();
        }
    }

    public void hideCustomView() {
        try {
            mChromeClient.onHideCustomView();
        } catch (Exception e) {
        }
    }

    public boolean inCustomView() {
        return (mCustomView != null);
    }

    private void hideStatusBar(boolean isShow) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            if (isShow) {
                View decorView = getWindow().getDecorView();
                // Hide Status Bar.
                int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                decorView.setSystemUiVisibility(uiOptions);
            } else {
                View decorView = getWindow().getDecorView();
                // Show Status Bar.
                int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
                decorView.setSystemUiVisibility(uiOptions);
            }
        } else {
            if (isShow) {
                // Hide Status Bar.
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else {
                // Show Status Bar.
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }
        }
    }

    @Click
    void btnWebViewBack() {
        onBackPressed();
    }
}
