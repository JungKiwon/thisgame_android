package com.howgame.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.reflect.TypeToken;
import com.howgame.android.R;
import com.howgame.android.activity.DetailActivity_;
import com.howgame.android.api.BaseApi;
import com.howgame.android.model.GameDetailModel;
import com.howgame.android.model.RecommendListsModel;
import com.howgame.android.property.Constants;
import com.howgame.android.property.NetworkProperty;
import com.howgame.android.util.LogUtil;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.net.URLDecoder;
import java.text.NumberFormat;

/**
 * Created by Administrator on 2016-08-23.
 */
public class MainListAdapter extends BaseAdapter {

    private boolean isMain = true;

    public MainListAdapter(Context context) {
        super(context);
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_1;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);

        if (!canGo())
            return;

        final RecommendListsModel.RecommendList item = (RecommendListsModel.RecommendList) mList.get(position);
        LinearLayout llList = mHolder.getAsLinearLayout(R.id.llList);
        ImageView ivImage = mHolder.getAsImageView(R.id.ivImage);
        TextView tvPublisher = mHolder.getAsTextView(R.id.tvPublisher);
        ImageView ivStar1 = mHolder.getAsImageView(R.id.ivStar1);
        ImageView ivStar2 = mHolder.getAsImageView(R.id.ivStar2);
        ImageView ivStar3 = mHolder.getAsImageView(R.id.ivStar3);
        ImageView ivStar4 = mHolder.getAsImageView(R.id.ivStar4);
        ImageView ivStar5 = mHolder.getAsImageView(R.id.ivStar5);
        TextView tvCount = mHolder.getAsTextView(R.id.tvCount);
        TextView tvCategory = mHolder.getAsTextView(R.id.tvCategory);
        TextView tvRecommend = mHolder.getAsTextView(R.id.tvRecommend);
        LinearLayout llRecommend = mHolder.getAsLinearLayout(R.id.llRecommend);

        if (item.getThumbnail_url() != null && item.getThumbnail_url().length() > 0) {
            Glide.with(mContext)
                    .load(URLDecoder.decode(item.getThumbnail_url()))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivImage);
        }

        TextView tvTitle = mHolder.getAsTextView(R.id.tvTitle);

        if (item.getTitle() != null && item.getTitle().length() > 0) {
            tvTitle.setText(item.getTitle());
            LogUtil.d(position + " title : " + item.getTitle());
        }

        if (item.getCompany() != null && item.getCompany().length() > 0) {
            tvPublisher.setText(item.getCompany());
            LogUtil.d(position + " getCompany : " + item.getCompany());
        }

        if (item.getRating_value() >= 5.0f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_s);
            ivStar3.setBackgroundResource(R.drawable.ico_star_s);
            ivStar4.setBackgroundResource(R.drawable.ico_star_s);
            ivStar5.setBackgroundResource(R.drawable.ico_star_s);
        } else if (item.getRating_value() >= 4.5f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_s);
            ivStar3.setBackgroundResource(R.drawable.ico_star_s);
            ivStar4.setBackgroundResource(R.drawable.ico_star_s);
            ivStar5.setBackgroundResource(R.drawable.ico_star_half_s);
        } else if (item.getRating_value() >= 4.0f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_s);
            ivStar3.setBackgroundResource(R.drawable.ico_star_s);
            ivStar4.setBackgroundResource(R.drawable.ico_star_s);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        } else if (item.getRating_value() >= 3.5f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_s);
            ivStar3.setBackgroundResource(R.drawable.ico_star_s);
            ivStar4.setBackgroundResource(R.drawable.ico_star_half_s);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        } else if (item.getRating_value() >= 3.0f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_s);
            ivStar3.setBackgroundResource(R.drawable.ico_star_s);
            ivStar4.setBackgroundResource(R.drawable.ico_star_e);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        } else if (item.getRating_value() >= 2.5f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_s);
            ivStar3.setBackgroundResource(R.drawable.ico_star_half_s);
            ivStar4.setBackgroundResource(R.drawable.ico_star_e);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        } else if (item.getRating_value() >= 2.0f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_s);
            ivStar3.setBackgroundResource(R.drawable.ico_star_e);
            ivStar4.setBackgroundResource(R.drawable.ico_star_e);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        } else if (item.getRating_value() >= 1.5f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_half_s);
            ivStar3.setBackgroundResource(R.drawable.ico_star_e);
            ivStar4.setBackgroundResource(R.drawable.ico_star_e);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        } else if (item.getRating_value() >= 1.0f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_e);
            ivStar3.setBackgroundResource(R.drawable.ico_star_e);
            ivStar4.setBackgroundResource(R.drawable.ico_star_e);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        } else if (item.getRating_value() >= 0.5f) {
            ivStar1.setBackgroundResource(R.drawable.ico_star_half_s);
            ivStar2.setBackgroundResource(R.drawable.ico_star_e);
            ivStar3.setBackgroundResource(R.drawable.ico_star_e);
            ivStar4.setBackgroundResource(R.drawable.ico_star_e);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        } else {
            ivStar1.setBackgroundResource(R.drawable.ico_star_e);
            ivStar2.setBackgroundResource(R.drawable.ico_star_e);
            ivStar3.setBackgroundResource(R.drawable.ico_star_e);
            ivStar4.setBackgroundResource(R.drawable.ico_star_e);
            ivStar5.setBackgroundResource(R.drawable.ico_star_e);
        }

        if (item.getRating_count() != null && item.getRating_count().length() > 0) {
            String count = item.getRating_count().replace(",", "").trim();
            tvCount.setText(NumberFormat.getInstance().format(Integer.parseInt(count)));
            LogUtil.d(position + " getRating_count : " + item.getRating_count());
        }

        if (item.getCategory() != null && item.getCategory().length() > 0) {
            tvCategory.setText(item.getCategory());
        }

        if (item.getRecommend_message() != null && item.getRecommend_message().length() > 0 && item.getOrig_title() != null && item.getOrig_title().length() > 0) {
            llRecommend.setVisibility(View.VISIBLE);
            SpannableStringBuilder sBuilder = new SpannableStringBuilder(item.getOrig_title() + item.getRecommend_message());
            sBuilder.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.download)), 0, item.getOrig_title().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvRecommend.setText(sBuilder, TextView.BufferType.SPANNABLE);
        } else {
            llRecommend.setVisibility(View.VISIBLE);
            String title = mContext.getResources().getString(R.string.ad_title) + "\n";
            String msg = mContext.getResources().getString(R.string.app_name);
            SpannableStringBuilder sBuilder = new SpannableStringBuilder(title + msg);
            sBuilder.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.download)), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvRecommend.setText(sBuilder, TextView.BufferType.SPANNABLE);
//            llRecommend.setVisibility(View.GONE);
        }
        if (item.getPackage_name() != null && item.getPackage_name().length() > 0) {
            llList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendClick(item.getPackage_name());
                    DetailActivity_.intent(mContext).mPackageName(item.getPackage_name()).start();
                }
            });
        }
    }

    private void sendClick(String packageName) {
        Ion.with(mContext)
                .load(NetworkProperty.HOST + NetworkProperty.API_GAME_CLICK)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                .setBodyParameter("package_name", packageName)
                .as(new TypeToken<BaseApi>() {
                })
                .setCallback(new FutureCallback<BaseApi>() {
                    @Override
                    public void onCompleted(Exception e, BaseApi ret) {
                        if (e != null) {
                            LogUtil.e(e);
                        }

                        if (ret != null) {
                            try {
                                LogUtil.d("sendClick msg : " + ret.getError_message());
                            } catch (Exception ex) {
                            }
                        }
                    }
                });
    }
}
