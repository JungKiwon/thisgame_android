package com.howgame.android.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.google.gson.reflect.TypeToken;
import com.howgame.android.R;
import com.howgame.android.activity.DetailActivity_;
import com.howgame.android.adapter.BaseAdapter;
import com.howgame.android.adapter.MainListAdapter;
import com.howgame.android.api.BaseApi;
import com.howgame.android.api.RecommendListsApi;
import com.howgame.android.api.TopListsApi;
import com.howgame.android.model.TopListsModel;
import com.howgame.android.property.Constants;
import com.howgame.android.property.NetworkProperty;
import com.howgame.android.util.HowGameUtil;
import com.howgame.android.util.LogUtil;
import com.howgame.android.view.MainListMarginDecoration;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.viewpagerindicator.IconPageIndicator;
import com.viewpagerindicator.IconPagerAdapter;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016-08-23.
 */
@EFragment(R.layout.fragment_main)
public class MainListFragment extends BaseFragment {

    @ViewById
    ViewPager viewPager;

    @ViewById
    IconPageIndicator viewPagerIndicator;

    @ViewById
    ObservableRecyclerView rvRelativeGame;

    @ViewById
    RelativeLayout rlRefresh, rlAd;

    @ViewById
    ImageButton btnRefresh;

    @ViewById
    Button btnSetting;

    @ViewById
    NestedScrollView svMain;


    private CountDownTimer mCountDownTimer;
    private MainListAdapter mMainAdapter;


    private long time = 5000;
    private boolean isScrolled = false;

    private Handler checkScroll;
    private boolean isFirstRun = true;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initLayout();
        getTopList();
        initList();
    }

    private void initViewPager(ArrayList<TopListsModel.TopInfos> dataSet) {
        viewPager.setAdapter(new TopListPagerAdapter(dataSet));
        viewPagerIndicator.setViewPager(viewPager);
    }

    private void initLayout() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            RelativeLayout.LayoutParams paramsSetting = new RelativeLayout.LayoutParams(HowGameUtil.getPX(mActivity, 20), HowGameUtil.getPX(mActivity, 22));
            paramsSetting.setMargins(0, HowGameUtil.getPX(mActivity, 36), HowGameUtil.getPX(mActivity, 12), 0);
            paramsSetting.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            btnSetting.setLayoutParams(paramsSetting);
            checkScroll = new Handler();
        }
    }

    private void initList() {
        mMainAdapter = new MainListAdapter(mActivity);
        mMainAdapter.setPrefManager(mPrefsManager);
        GridLayoutManager relativeLayoutManager = new GridLayoutManager(mActivity, 2);
        relativeLayoutManager.setAutoMeasureEnabled(true);
        relativeLayoutManager.setOrientation(GridLayout.VERTICAL);
        relativeLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int type = mMainAdapter.getItemViewType(position);
                if (type == BaseAdapter.VIEW_TYPE_HEADER) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        rvRelativeGame.addItemDecoration(new MainListMarginDecoration(mActivity, 8));
        rvRelativeGame.setNestedScrollingEnabled(false);
        rvRelativeGame.setHasFixedSize(false);
        rvRelativeGame.setLayoutManager(relativeLayoutManager);
        mMainAdapter.setResourceId(R.layout.item_main_list);
        mMainAdapter.setRecyclerView(rvRelativeGame);
        rlRefresh.setAlpha(0.0f);
        svMain.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                rlRefresh.setAlpha(0.0f);
                View view = (View) svMain.getChildAt(svMain.getChildCount() - 1);
                int diff = (view.getBottom() - (svMain.getHeight() + svMain.getScrollY()));
                if (diff <= 10) {
                    if (mMainAdapter.getList().size() > 0)
                        stopAndStartRepeatingTask();
                    LogUtil.d("end of scroll");
                    rlRefresh.setVisibility(View.VISIBLE);
                    btnRefresh.performClick();
                    btnRefresh.performClick();
                    btnRefresh.setPressed(true);
                    btnRefresh.invalidate();
                    btnRefresh.postDelayed(new Runnable() {
                        public void run() {
                            btnRefresh.setPressed(false);
                            btnRefresh.invalidate();
                        }
                    }, 800);
                } else {
                    rlRefresh.setVisibility(View.GONE);
                }
            }
        });
        rvRelativeGame.setAdapter(mMainAdapter);
    }

    private void getTopList() {
        LogUtil.d("getTopList url : " + NetworkProperty.HOST + NetworkProperty.API_GAME_TOP_LISTS);
        LogUtil.d("getTopList auth_token : " + mPrefsManager.authToken().get());
        show(getString(R.string.loading_recommend_list));
        Ion.with(this)
                .load(NetworkProperty.HOST + NetworkProperty.API_GAME_TOP_LISTS)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                .as(new TypeToken<TopListsApi>() {
                })
                .setCallback(new FutureCallback<TopListsApi>() {
                    @Override
                    public void onCompleted(Exception e, TopListsApi ret) {
//                        dismiss();
                        if (e != null) {
                            LogUtil.e(e);
                            rlAd.setVisibility(View.GONE);
                            getRecommendList();
                        }

                        if (ret != null) {
//                            LogUtil.d("getTopList msg : " + ret.getError_message());
//                            LogUtil.d("getTopList msg : " + ret.getResult().toPrettyJson());
//                            LogUtil.d("getTopList msg : " + ret.getResult().toPrettyJson());
                            if (ret.getResult() != null) {
                                if (ret.getResult().getTop_infos() != null && ret.getResult().getTop_infos().size() > 0) {
                                    initViewPager(ret.getResult().getTop_infos());
                                    getRecommendList();
                                } else {
                                    rlAd.setVisibility(View.GONE);
                                    getRecommendList();
                                }
                            } else {
                                rlAd.setVisibility(View.GONE);
                                getRecommendList();
                            }
                        } else {
                            rlAd.setVisibility(View.GONE);
                            getRecommendList();


                        }
                    }
                });
    }

    private void getRecommendList() {
        LogUtil.d("getRecommendList url : " + NetworkProperty.HOST + NetworkProperty.API_GAME_RECOMMEND_LISTS);
        LogUtil.d("getRecommendList auth_token : " + mPrefsManager.authToken().get());
//        show(getString(R.string.loading_recommend_list));
        Ion.with(this)
                .load(NetworkProperty.HOST + NetworkProperty.API_GAME_RECOMMEND_LISTS)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                .setBodyParameter("cursor", getNextCursor())
                .as(new TypeToken<RecommendListsApi>() {
                })
                .setCallback(new FutureCallback<RecommendListsApi>() {
                    @Override
                    public void onCompleted(Exception e, RecommendListsApi ret) {
                        dismiss();
                        if (e != null) {
                            LogUtil.e(e);
                        }

                        if (ret != null) {
                            LogUtil.d("getRecommendList msg : " + ret.getError_message());
                            LogUtil.d("getRecommendList msg : " + ret.getResult().toPrettyJson());
                            setNextCursor(String.valueOf(ret.getResult().getCursor_next()));
                            checkEndPage();

                            mMainAdapter.addAll(ret.getResult().getRecommend_list());
                            mMainAdapter.notifyDataSetChanged();
                            LogUtil.d("rlRefresh height : " + rlRefresh.getHeight());
                            LogUtil.d("svMain height : " + svMain.getMeasuredHeight());
                            if (rlAd.getVisibility() == View.GONE && mMainAdapter.getList().size() < 5) {
                                rlRefresh.setAlpha(1.0f);
                                rlRefresh.setVisibility(View.VISIBLE);
                            } else if (rlAd.getVisibility() == View.VISIBLE && mMainAdapter.getList().size() < 3) {
                                rlRefresh.setAlpha(1.0f);
                                rlRefresh.setVisibility(View.VISIBLE);
                            } else {
                                rlRefresh.setAlpha(0.0f);
                                rlRefresh.setVisibility(View.GONE);
                            }

                        }
                    }
                });
    }


    /**
     * 포커스 테마 페이져 어댑터
     */
    public class TopListPagerAdapter extends PagerAdapter implements IconPagerAdapter {

        ArrayList<TopListsModel.TopInfos> mDataSet;
        private LayoutInflater inflater;

        public TopListPagerAdapter(ArrayList<TopListsModel.TopInfos> mDataSet) {
            this.mDataSet = mDataSet;
            this.inflater = LayoutInflater.from(mActivity);


            if (mCountDownTimer != null)
                mCountDownTimer.cancel();

            mCountDownTimer = new CountDownTimer(3000, 3000) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {

                    if (viewPager == null || !viewPager.isShown())
                        return;

                    int count = viewPager.getAdapter().getCount();
                    int pos = viewPager.getCurrentItem();

                    if (pos == count - 1) {
                        viewPager.setCurrentItem(0);
                    } else {
                        viewPager.setCurrentItem(pos + 1, true);
                    }
                    start();
                }
            }.start();
        }

        @Override
        public int getIconResId(int index) {
            return R.drawable.selector_indicator;
        }

        @Override
        public int getCount() {
            return this.mDataSet.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.game_thumbnail_item, container, false);
            ImageView ivThumbnail = (ImageView) view.findViewById(R.id.ivThumbnail);
            final TopListsModel.TopInfos item = mDataSet.get(position);
            if (item.getThumbnail_url() != null && item.getThumbnail_url().length() > 0) {
                Glide.with(mContext)
                        .load(URLDecoder.decode(item.getThumbnail_url()))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(ivThumbnail);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendClick(item.getPackage_name());
                    DetailActivity_.intent(mContext).flags(Intent.FLAG_ACTIVITY_NEW_TASK).mPackageName(item.getPackage_name()).start();
                }
            });

            container.addView(view);

            return view;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    @Click
    void rlRefresh() {
        rlRefresh.setVisibility(View.GONE);
        btnRefresh.performClick();
//        setNextCursor(null);
        mMainAdapter.clear();
        getRecommendList();
    }

    @Click
    void btnSetting() {
        final String items[] = getResources().getStringArray(R.array.push);
        AlertDialog.Builder ab = new AlertDialog.Builder(mActivity);
        ab.setTitle(getString(R.string.push_title));
        ab.setSingleChoiceItems(items, mPrefsManager.pushOn().get() ? 0 : 1,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        LogUtil.d("setSingleChoiceItems whichButton : " + whichButton);
                        if (whichButton == 0) {
                            mPrefsManager.pushOn().put(true);
                        } else {
                            mPrefsManager.pushOn().put(false);
                        }
                    }
                }).setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        LogUtil.d("OnClickListener whichButton : " + whichButton);
                        if (mPrefsManager.pushOn().get()) {
                            sendToggleUsePush("Y");
                        } else {
                            sendToggleUsePush("N");
                        }
                        dismiss();
                    }
                }).setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Cancel 버튼 클릭시
                    }
                });
        ab.show();
    }

    private void sendToggleUsePush(String yn) {
        LogUtil.d("sendToggleUsePush url : " + NetworkProperty.HOST + NetworkProperty.API_DEVICE_TOGGLE_USE_PUSH);
        LogUtil.d("sendToggleUsePush auth_token : " + mPrefsManager.authToken().get());
        LogUtil.d("sendToggleUsePush yn : " + yn);
        Ion.with(this)
                .load(NetworkProperty.HOST + NetworkProperty.API_DEVICE_TOGGLE_USE_PUSH)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                .setBodyParameter("is_use", yn)
                .as(new TypeToken<RecommendListsApi>() {
                })
                .setCallback(new FutureCallback<RecommendListsApi>() {
                    @Override
                    public void onCompleted(Exception e, RecommendListsApi ret) {
                        dismiss();
                        if (e != null) {
                            LogUtil.e(e);
                        }

                        if (ret != null) {
                            LogUtil.d(ret.getError_message());
                        }
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCountDownTimer != null) {
            mCountDownTimer.start();
        }
    }

    private void sendClick(String packageName) {
        Ion.with(mContext)
                .load(NetworkProperty.HOST + NetworkProperty.API_GAME_CLICK)
                .setTimeout(Constants.TIME_OUT)
                .setBodyParameter("auth_token", mPrefsManager.authToken().get())
                .setBodyParameter("package_name", packageName)
                .as(new TypeToken<BaseApi>() {
                })
                .setCallback(new FutureCallback<BaseApi>() {
                    @Override
                    public void onCompleted(Exception e, BaseApi ret) {
                        if (e != null) {
                            LogUtil.e(e);
                        }

                        if (ret != null) {
                            try {
                                LogUtil.d("sendClick msg : " + ret.getError_message());
                            } catch (Exception ex) {
                            }
                        }
                    }
                });
    }


    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                rlRefresh.setAlpha(1.0f);
                TranslateAnimation trans = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0
                        , Animation.RELATIVE_TO_SELF, 0
                        , Animation.RELATIVE_TO_SELF, 1
                        , Animation.RELATIVE_TO_SELF, 0);
                trans.setDuration(500);

                AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
                alpha.setDuration(500);
//
                AnimationSet animationSet = new AnimationSet(true);
                animationSet.addAnimation(alpha);
                animationSet.addAnimation(trans);
                rlRefresh.clearAnimation();
                rlRefresh.startAnimation(animationSet);

            } finally {
//                checkScroll.postDelayed(mStatusChecker, 1000);
            }
        }
    };

    void stopAndStartRepeatingTask() {
        checkScroll.removeCallbacks(mStatusChecker);
        checkScroll.postDelayed(mStatusChecker, 100);
    }

    void stopRepeatingTask() {
        LogUtil.d("stopRepeatingTask");
        checkScroll.removeCallbacks(mStatusChecker);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        stopRepeatingTask();
    }
}
