package com.howgame.android.model;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-09-20.
 */
public class RecommendListsModel extends BaseModel {
    private ArrayList<RecommendList> recommend_list;//	Object	O	게임 정보
    private int cursor_next;
    private int total_count;

    public ArrayList<RecommendList> getRecommend_list() {
        return recommend_list;
    }

    public void setRecommend_list(ArrayList<RecommendList> recommend_list) {
        this.recommend_list = recommend_list;
    }

    public int getCursor_next() {
        return cursor_next;
    }

    public void setCursor_next(int cursor_next) {
        this.cursor_next = cursor_next;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public class RecommendList {
        private String package_name;//	string	O	패키지명
        private String title;//	string	O	게임명
        private String company;//	string	O	제작사
        private float rating_value;//		O	평점
        private String rating_count;//	string	O	평점 참여수
        private String category;//	string	O	장르
        private String thumbnail_url;//	string	O	게임 로고 UrL
        private String recommend_message;//	string	O	추천 메시지
        private String orig_title;

        public String getPackage_name() {
            return package_name;
        }

        public void setPackage_name(String package_name) {
            this.package_name = package_name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public float getRating_value() {
            return rating_value;
        }

        public void setRating_value(float rating_value) {
            this.rating_value = rating_value;
        }

        public String getRating_count() {
            return rating_count;
        }

        public void setRating_count(String rating_count) {
            this.rating_count = rating_count;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getThumbnail_url() {
            return thumbnail_url;
        }

        public void setThumbnail_url(String thumbnail_url) {
            this.thumbnail_url = thumbnail_url;
        }

        public String getRecommend_message() {
            return recommend_message;
        }

        public void setRecommend_message(String recommend_message) {
            this.recommend_message = recommend_message;
        }

        public String getOrig_title() {
            return orig_title;
        }

        public void setOrig_title(String orig_title) {
            this.orig_title = orig_title;
        }
    }

}
