package com.howgame.android.property;

/**
 * Created by Administrator on 2016-09-20.
 */
public class Constants {
    public static final int TIME_OUT = 5000;
    public static final String OS_VERSION = android.os.Build.VERSION.RELEASE;
    public static final String PLATFORM = "1";
    public static final String FIREBASE_ANALYTICS_ITEM_SPALSH = "SplashActivity";
    public static final String FIREBASE_ANALYTICS_ITEM_MAIN = "MainActivity";
    public static final String FIREBASE_ANALYTICS_ITEM_DETAIL = "DetailActivity";
    public static final String FIREBASE_ANALYTICS_ITEM_WEB = "WebActivity";
    public static final String FIREBASE_ANALYTICS_ITEM_GOOGLE_PLAY = "GooglePlay";

    public static final int SEND_INSTALL_PACKAGE_TIME = 60 * 60 * 1000;

    public static final String YOUTUBE_API_KEY = "AIzaSyAyZnvjsDssLVuI5Dm3Sg0G6TKnEb5s_pk";

    public static final String AES_KEY = "nvista1234567890abcdefghigklmnop";
}
