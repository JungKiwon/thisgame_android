package com.howgame.android.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.howgame.android.R;

/**
 * Created by Administrator on 2016-08-22.
 */
public class HowGameProgressDialog extends android.app.ProgressDialog {

    private ImageView ivLoading;
    private TextView tvLoading;

    public HowGameProgressDialog(Context context, String msg) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setMessage(msg);
//        setContentView(R.layout.loading_dialog);
//        ivLoading = (ImageView) findViewById(R.id.ivLoading);
//        tvLoading = (TextView) findViewById(R.id.tvLoading);
//        tvLoading.setText(msg);
//        setCancelable(false);
//        Glide.with(context).load(R.drawable.loading_ani)
//                .asGif()
//                .dontTransform()
//                .into(ivLoading);
    }

    public HowGameProgressDialog(Context context, int themeResId) {
        super(context, themeResId);
    }


}
